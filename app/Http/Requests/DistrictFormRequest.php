<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class DistrictFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'name_ar'                        => 'required|string|unique:districts,name_ar',
                    'name_en'                        => 'required|string|unique:districts,name_en',
                    'name_ur'                        => 'required|string|unique:districts,name_ur',
                    'code'                           => 'required|string|unique:districts,code',
                    'city_id'                        => 'required|int|exists:cities,id',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name_en'                         => 'required|string|unique:districts,name_en,'.$id,
                    'name_ar'                         => 'required|string|unique:districts,name_ar,'.$id,
                    'name_ur'                         => 'required|string|unique:districts,name_ur,'.$id,
                    'code'                            => 'required|unique:districts,code,'.$id,
                    'city_id'                         => 'required|unique:cities,name_ur,'.$id,
                ];
            }
            default:break;
        }
    }
}
