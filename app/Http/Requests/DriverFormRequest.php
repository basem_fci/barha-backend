<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class DriverFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->segment(3) == 'change-password'){
            return [
                'password'      =>  'required|string|min:6|confirmed'
            ];
        }

        $id = $this->segment(3);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'name'                          => 'required|string',
                    'email'                         => 'required|string|email|unique:users,email',
                    'mobile'                        => 'required|string|unique:users,mobile',
                    'password'                      => 'required|string|min:6|confirmed',
                    'type'                          => 'required|string|in:company,driver',
                    'status'                        => 'required|string|in:active,in-active',
                    'nationality'                   => 'required|string|in:saudi,another',
                    'picture'                       => 'nullable|image',
                    'picture_car_registration_form' => 'nullable|image',
                    'picture_resident_id'           => 'nullable|image',
                    'picture_working_card_transport'=> 'nullable|image',
                    'picture_driver_license'        => 'nullable|image'
                ];

            }
            case 'PUT':
            case 'PATCH':
            {

                return [
                    'name'                          => 'required|string',
                    'email'                         => 'required|string|email|unique:users,email,'.$id,
                    'mobile'                        => 'required|string|unique:users,mobile,'.$id,
                    'password'                      => 'nullable|string|min:6|confirmed',
                    'type'                          => 'required|string|in:company,driver',
                    'status'                        => 'required|string|in:active,in-active',
                    'nationality'                   => 'required|string|in:saudi,another',
                    'picture'                       => 'nullable|image',
                    'picture_car_registration_form' => 'nullable|image',
                    'picture_resident_id'           => 'nullable|image',
                    'picture_working_card_transport'=> 'nullable|image',
                    'picture_driver_license'        => 'nullable|image'
                ];
            }
            default:break;
        }
    }
}
