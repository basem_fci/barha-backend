<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->segment(1) == 'change-password'){
            return [
                'password'      =>  'required|string|min:6|confirmed'
            ];
        }

        $segment =$this->segments();
        if ($segment[0] =='system')
        {
            $id = $this->segment(3);
        }else{
            $id = $this->segment(2);
        }
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'name'                          => 'required|string',
                    'email'                         => 'nullable|string|email|unique:users,email',
                    'mobile'                        => 'required|string|unique:users,mobile',
                    'password'                      => 'required|string|min:6|confirmed',
                    'picture'                       => 'nullable|image',
                ];

            }
            case 'PUT':
            case 'PATCH':
            {

                return [
                    'name'                          => 'required|string',
                    'email'                         => 'nullable|string|email|unique:users,email,'.$id,
                    'mobile'                        => 'required|string|unique:users,mobile,'.$id,
                    'picture'                       => 'nullable|image',
                ];
            }
            default:break;
        }
    }
}
