<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class CityFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'name_ar'                        => 'required|string|unique:cities,name_ar',
                    'name_en'                        => 'required|string|unique:cities,name_en',
                    'name_ur'                        => 'required|string|unique:cities,name_ur',
                ];

            }
            case 'PUT':
            case 'PATCH':
            {

                return [
                    'name_en'                         => 'required|string|unique:cities,name_en,'.$id,
                    'name_ar'                         => 'required|string|unique:cities,name_ar,'.$id,
                    'name_ur'                         => 'required|string|unique:cities,name_ur,'.$id,
                ];
            }
            default:break;
        }
    }
}
