<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class CompanyFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->segment(3) == 'change-password'){
            return [
                'password'      =>  'required|string|min:6|confirmed'
            ];
        }
        $id = $this->segment(3);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'name'                          => 'required|string',
                    'mobile'                        => 'required|string|unique:users,mobile',
                    'download_manager_number'       => 'required|string|unique:users,mobile',
                    'logistic_manager_number'       => 'required|string|unique:users,mobile',
                    'password'                      => 'required|string|min:6|confirmed',
                    'type'                          => 'required|string|in:company,driver',
                    'status'                        => 'required|string|in:active,in-active',
                    'picture'                       => 'nullable|image',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'                          => 'required|string',
                    'mobile'                        => 'required|string|unique:users,mobile,'.$id,
                    'download_manager_number'       => 'required|string|unique:users,mobile,'.$id,
                    'logistic_manager_number'       => 'required|string|unique:users,mobile,'.$id,
                    'password'                      => 'nullable|string|min:6|confirmed',
                    'type'                          => 'required|string|in:company,driver',
                    'status'                        => 'required|string|in:active,in-active',
                    'picture'                       => 'nullable|image'
                ];
            }
            default:break;
        }
    }
}
