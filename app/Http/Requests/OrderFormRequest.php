<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rowid = $this->segment(2);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST': {
                return [
                    'city_id_from' => 'required',
                    'city_id_to' => 'required',
                    'district_id_from' => 'required',
                    'district_id_to' => 'required',
                    'creatable_id' => 'required',
                    'creatable_type' => 'required',
                    'payload_type' => 'required',
                    'price' => 'required',
                    'tracking_number' => 'required',
                    'deliver_date' => 'required',
                    'address' => 'required',
                ];

            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'city_id_from' => 'required',
                    'city_id_to' => 'required',
                    'district_id_from' => 'required',
                    'district_id_to' => 'required',
                    'creatable_id' => 'required',
                    'creatable_type' => 'required',
                    'payload_type' => 'required',
                    'price' => 'required',
                    'tracking_number' => 'required',
                    'deliver_date' => 'required',
                    'address' => 'required',
                ];
            }
            default:break;
        }

    }
}
