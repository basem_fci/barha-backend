<?php

namespace App\Http\Middleware;

use App\Models\Staff;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::guard('staff')->check()) {
            $auth = Auth::guard('staff')->user();
            if(in_array($request->language,['ar','en','ur'])){
                Staff::find(Auth::id())->update([
                    'language'=> $request->language
                ]);
                \App::setLocale($request->language);

                if($request->backByLanguage){
                    return redirect()->back();
                }

            }else{
                \App::setLocale($auth->language);
            }
        }else{
            if(isset($request->lang) && in_array($request->lang,['ar','en','ur'])) {
                \App::setLocale($request->lang);
                session()->put('locale',$request->lang);
            }else {
                if (!session()->get('locale'))
                {
                    session()->put('locale','ar');
                }
                \App::setLocale(session()->get('locale'));
            }
        }



        return $next($request);
    }
}
