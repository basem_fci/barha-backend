<?php

namespace App\Http\Middleware;

use App\Models\Staff;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LanguageWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(Session::get('lang'))) {
            $locale = Session::get('lang');//Session::get('lang',config::get('app.locale'));
        } else {
            $locale = 'ar';
        }
        \App::setLocale($locale);
        return $next($request);
    }
}
