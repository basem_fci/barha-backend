<?php

namespace App\Modules\System;

 use App\Models\Driver;
 use App\Models\Staff;
 use App\Models\PermissionGroup;
 use App\Models\User;
use Illuminate\Http\Request;
use Form;
use Auth;
use App;

class AjaxController extends SystemController{

    public function index(Request $request){

        switch ($request->type) {


            case 'available_driver':
                $drivers = User::where('id',$request->driver_id)->get();
                dd($drivers);
                return  $drivers;

                break;

                case 'all_available_driver':
                $online_driver_from_app = AuthApi::where('guard_name','driver_api')->pluck('user_id');
                if($online_driver_from_app->IsEmpty()){
                    return [];
                }else{
                    $driver_ids = $online_driver_from_app->toArray();
                    $online_driver_from_web = App\Models\AuthSession::where('guard_name','driver')->pluck('user_id');
                    if($online_driver_from_web->isNotEmpty()){
                        $driver_ids =   array_merge($online_driver_from_web->toArray(),$driver_ids);
                    }
                    $drivers = Driver::select('id','name','lat','lng','available','plan_id',
                        \DB::raw('(CASE WHEN id in ('.setting("escape_drivers_from_auto_assign").') THEN "yes" ELSE "no" END)  as escape')
                    )
                        ->whereIn('id',$driver_ids)
                        -> withCount(['orders'=>function($q){
                            return  $q->whereIn('status',['accepted','arrived','collected']);
                        }])->get();
                    return  $drivers;
                }
                break;
            case 'saveLog':
                if (!empty($request->desc) && !empty($request->model) && !empty($request->id)) {
                    save_log(__($request->desc), $request->model, $request->id);
                    return [];
                }
                return [];
                break;

            case 'readNotification':
                foreach (Auth::user()->unreadNotifications as $notification) {
                    $notification->markAsRead();
                }
                break;

            case 'getNextAreas':
                return AreasData::getNextAreas($request->id);
                break;


            case 'dropdownMenuArea':
                $id = $request->id;


                if ($id == 0) {
                    return [
                        'area_type_id' => 1,
                        'areas' => Area::where('area_type_id', 1)->get(['id', 'name_' . \App::getLocale() . ' as name'])
                    ];
                }

                $data = AreasData::getNextAreas($id);

                $returnData = [];
                if (!empty($data['areas'])) {
                    foreach ($data['areas'] as $key => $value) {
                        $returnData[] = [
                            'id' => $value['id'],
                            'name' => $value['name_' . \App::getLocale()]
                        ];
                    }

                    return [
                        'area_type_id' => $data['type']->id,
                        'areas' => $returnData
                    ];
                }

                return [];

                break;

            case 'drivers':
                $word = $request->word;

                $data = Driver::where('status', 'active')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name', 'mobile']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;

            case 'vendors':
                $word = $request->word;
                $data = User::where('status', 'active')
                    ->where('type','company')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;
            case 'users':
                $word = $request->word;

                $data =  User::where('status', 'active')
                    ->where('type','person')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;


            case 'staff':
                $word = $request->word;


                $data = Staff::where('status', 'active')
                    ->where(function ($query) use ($word) {
                        $query->where('firstname', 'LIKE', '%' . $word . '%')
                            ->orWhere('lastname', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id',
                        \DB::raw('CONCAT(firstname," ",lastname) as value')
                    ]);

                if(!$data) return [];
                return $data;

//                $returnData = [];
//                foreach ($data as $key => $value){
//                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->firstname.' '.$value->lastname];
//                }
//
//                return $returnData;

                break;
            case 'area':
                $word = $request->word;

                $data = Area::where(function($query) use ($word) {
                    $query->where('name_ar','LIKE','%'.$word.'%')
                        ->orWhere('name_en','LIKE','%'.$word.'%');
                })->get([
                    'id'
                ]);

                if($data->isEmpty()){
                    return [];
                }

                $result = [];

                foreach ($data as $key => $value){
                    $result[] = [
                        'id'=> $value->id,
                        'value'=> str_replace($word,'<b>'.$word.'</b>',implode(' -> ',AreasData::getAreasUp($value->id,true) ))
                    ];

                    if(setting('area_select_type') == '2'){
                        $areaDown = AreasData::getAreasDown($value->id);
                        if(count($areaDown) > 1){
                            array_shift($areaDown);
                            foreach ($areaDown as $aK => $aV){
                                $result[] = [
                                    'id'=> $aV,
                                    'value'=> str_replace($word,'<b>'.$word.'</b>',implode(' -> ',AreasData::getAreasUp($aV,true) ))
                                ];
                            }
                        }
                    }

                }

                return $result;

                break;


        }

    }

}
