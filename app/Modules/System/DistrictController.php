<?php

namespace App\Modules\System;

use App\models\City;
use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Requests\DistrictFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class DistrictController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = District::select([
                'id',
                'city_id',
                'name_'.clang().' as name'
            ]);
            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }

            if ($request->downloadExcel == "true") {

                $excelData = $eloquentData;
                $excelData = $excelData->get();
                return  exportXLS(__('District'),
                    [
                        __('ID'),
                        __('District Name'),
                        __('City Name'),
                    ],
                    $excelData,
                    [
                        'id' => 'id',
                        'District Name' => 'name',
                        'City Name' => function($data){
                            return $data->city->{'name_'.clang()};
                        }
                    ]
                );
            }

            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('city_id', function($data){
                    return $data->city->{'name_'.clang()};
                })
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('action', function($data){
                    return $this->view('districts.action',compact('data'));
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('District Name'),
                __('City Name'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'districts.id',
                'name'=>'districts.name_'.clang(),
                'city_id'=>'districts.city_id',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('District')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add District'),
                'route'=>'system.districts.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;
            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted District');
            }else{
                $this->viewData['pageTitle'] = __('District');
            }
            $this->viewData['city'] = array_column(City::all()->toArray(),'name_ar','id');

            return $this->view('districts.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('District'),
            'url'=> route('system.districts.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create District'),
        ];

        $this->viewData['pageTitle'] = __('Create District');
        $this->viewData['city'] = City::all();


        return $this->view('districts.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictFormRequest $request){
        $requestData = $request->all();
        $insertData = District::create($requestData);
        if($insertData){
            return redirect()->route('system.districts.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\District  $districts
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request){

        $districts = District::find($id);
        if (!$districts){
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
        $this->viewData['breadcrumb'] = [
            [
                'text' => __('District'),
                'url' => route('system.districts.index'),
            ],
            [
                'text' => $districts->{'name_'.clang()},
            ]
        ];

        $this->viewData['pageTitle'] = __('District Profile');

        $this->viewData['result'] = $districts;
        return $this->view('districts.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district){

        if (!$district){
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('District'),
            'url'=> route('system.districts.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $district->{'name_'.clang()}]),
        ];

        $this->viewData['pageTitle'] = __('Edit District');
        $this->viewData['result'] = $district;
        $this->viewData['city'] = City::all();
        return $this->view('districts.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\District  $districts
     * @return \Illuminate\Http\Response
     */
    public function update(DistrictFormRequest $request, $id)
    {
        $districts = District::find($id);
        if (!$districts){
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
        $requestData = $request->all();
        $updateData = $districts->update($requestData);
        if($updateData){
            return redirect()->route('system.districts.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\District  $districts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $districts = District::find($id);
        if (!$districts){
            return redirect()->route('system.districts.index')->with('error', __('Sorry, we could not add the data'));
        }
        $districts->delete();
        return redirect()->route('system.districts.index')->with('delete',  __('District deleted successfully'));
    }


}
