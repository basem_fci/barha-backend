<?php

namespace App\Modules\System;

use App\models\City;
use App\models\District;
use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Requests\OrderFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class OrderController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->isDataTable){

            $eloquentData = Order::select([
                'id',
                'order_status_id',
                'driver_id',
                'city_id_from',
                'city_id_to',
                'district_id_from',
                'district_id_to',
                'cargo_weight',
                'payload_type',
                'total_price',
                'tracking_number',
                'address',
                'deliver_date',
            ]);

            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }

            whereBetween($eloquentData,'DATE(created_at)',$request->created_at1,$request->created_at2);

            if($request->id){
                $eloquentData->where('id','=',$request->id);
            }


            if ($request->downloadExcel == "true") {

                    $excelData = $eloquentData;
                    $excelData = $excelData->get();
                  return  exportXLS(__('Orders'),
                        [
                            __('ID'),
                            __('City From'),
                            __('City To'),
                            __('CargoWeight'),
                        ],
                        $excelData,
                        [
                            'id' => 'id',
                            'city_id_from' => 'city_id_from',
                            'city_id_to' => 'city_id_to',
                            'cargo_weight' => 'cargo_weight'
                        ]
                    );
                }

            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('city_id_from', function($data){
                    return $data->cityFrom->{'name_'.clang()};
                })
                ->addColumn('city_id_to', function($data){
                    return $data->cityTo->{'name_'.clang()};
                })
                ->addColumn('cargo_weight', function($data){
                    return $data->cargo_weight;
                })
                ->addColumn('action', function($data){
                    return $this->view('orders.action',compact('data'));
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('City From'),
                __('City To'),
//                __('District From'),
//                __('District To'),
                __('CargoWeight'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'orders.id',
                'city_id_from' => 'city_id_from',
                'city_id_to' => 'city_id_to',
                'cargo_weight' => 'cargo_weight',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Orders')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Order'),
                'route'=>'system.orders.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Order');
            }else{
                $this->viewData['pageTitle'] = __('Orders');
            }

            return $this->view('orders.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Orders'),
            'url'=> route('system.orders.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Order'),
        ];

        $this->viewData['pageTitle'] = __('Create Order');
        $this->viewData['city'] = City::all();
        return $this->view('orders.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderFormRequest $request){
        $requestData = $request->all();
        $requestData['total_price'] = (float)$request->price+(float)setting('value_added')+(float)setting('shipping_charges');
        $insertData = Order::create($requestData);
        if ($insertData->record(1,'Order Created successfully','App\Models\Staff')) {
            return redirect()->route('system.orders.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.orders.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $orders = Order::find($id);
        $this->viewData['breadcrumb'] = [
            [
                'text' => __('Orders'),
                'url' => route('system.orders.index'),
            ],
            [
                'text' => $orders->name,
            ]
        ];

        $this->viewData['pageTitle'] = __('View Order');


        $this->viewData['result'] = $orders;
        return $this->view('orders.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $orders = Order::find($id);
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text' => __('Order'),
            'url' => route('system.orders.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text' => __('Edit (:name)', ['name' => $orders->tracking_number]),
        ];

        $this->viewData['pageTitle'] = __('Edit Order');
        $this->viewData['result'] = $orders;
        $this->viewData['city'] = City::all();
        $this->viewData['district_from'] = District::whereCityId($orders->city_id_from)->get();
        $this->viewData['district_to'] = District::whereCityId($orders->city_id_to)->get();

        return $this->view('orders.create', $this->viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(OrderFormRequest $request,$id)
    {
        $orders = Order::find($id);
        $requestData = $request->all();
        $requestData['total_price'] = (float)$request->price+(float)setting('value_added')+(float)setting('shipping_charges');
        $updateData = $orders->update($requestData);
        if ($orders->record($orders->order_status_id,'Order Created successfully','App\Models\Staff')) {
            return redirect()->route('system.orders.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.orders.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $orders,Request $request)
    {
        $orders->delete();
        return redirect()->route('system.orders.index')->with('delete',  __('Order deleted successfully'));
    }

    public function district($id){
        $district = District::where('city_id',$id)->select('id','name_'.clang().' as name')->get();
        return $this->success('Done', $district);
    }
    protected function success($msg='Done',$data = []){
        return $this->response(true,$msg,$data);
    }

    protected function fail($msg='fail',$data = []){
        return $this->response(false,$msg,$data);
    }

    protected function response($status,$message = 'Done',$data = [],$code = '200'): array {
        return [
            'status'=> $status,
            'code'=> $code,
            'message'=> $message,
            'data'=> $data
        ];
    }
}
