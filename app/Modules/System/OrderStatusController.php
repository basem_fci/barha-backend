<?php

namespace App\Modules\System;

use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Requests\OrderStatusFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class OrderStatusController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = OrderStatus::select([
                'id',
                'name_'.clang().' as name',
            ]);
            // ->with('permission_group');

            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }


            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('action', function($data){
                    return $this->view('order_statuses.action',compact('data'));

                    return '<span class="dropdown">
                            <a href="#" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
                              <i class="la la-gear"></i>
                            </a>
                            <div class="dropdown-menu '.( (\App::getLocale() == 'ar') ? 'dropdown-menu-left' : 'dropdown-menu-right').'" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-36px, 25px, 0px);">
                                <a class="dropdown-item" href="'.route('system.order_statuses.show',$data->id).'" target="_blank"><i class="far fa-eye"></i> '.__('View').'</a>
                                <a class="dropdown-item" href="'.route('system.order_statuses.edit',$data->id).'"><i class="la la-edit"></i> '.__('Edit').'</a>
                                <a class="dropdown-item btn-danger" href="javascript:void(0);" onclick="deleteRecord(\''.route('system.order_statuses.destroy',$data->id).'\')"><i class="la la-trash-o"></i> '.__('Delete').'</a> 
                            </div>
                        </span>';
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Name'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'order_statuses.id',
                'name'=>'order_statuses.name',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Order Status')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Order Status'),
                'route'=>'system.order_statuses.create'
            ];

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Order Status');
            }else{
                $this->viewData['pageTitle'] = __('Order Status');
            }

            return $this->view('order_statuses.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Order Status'),
            'url'=> route('system.order_statuses.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Order Status'),
        ];

        $this->viewData['pageTitle'] = __('Create Order Status');


        return $this->view('order_statuses.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStatusFormRequest $request){
        $requestData = $request->all();
        $insertData = OrderStatus::create($requestData);
        if($insertData){
            return redirect()->route('system.order_statuses.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderStatus  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request){

        $order_statuses = OrderStatus::find($id);
        if (!$order_statuses){
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
        $this->viewData['breadcrumb'] = [
            [
                'text' => __('Order Status'),
                'url' => route('system.order_statuses.index'),
            ],
            [
                'text' => $order_statuses->{'name_'.clang()},
            ]
        ];

        $this->viewData['pageTitle'] = __('Order Status Profile');

        $this->viewData['result'] = $order_statuses;
        return $this->view('order_statuses.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $order_statuses = OrderStatus::find($id);
        if (!$order_statuses){
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Order Status'),
            'url'=> route('system.order_statuses.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $order_statuses->{'name_'.clang()}]),
        ];

        $this->viewData['pageTitle'] = __('Edit Order Status');
        $this->viewData['result'] = $order_statuses;

        return $this->view('order_statuses.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderStatus  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function update(OrderStatusFormRequest $request, $id)
    {
        $order_statuses = OrderStatus::find($id);
        if (!$order_statuses){
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
        $requestData = $request->all();
        $updateData = $order_statuses->update($requestData);
        if($updateData){
            return redirect()->route('system.order_statuses.index')->with('Updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderStatus  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        dd($id);
        $order_statuses = OrderStatus::find($id);
        if (!$order_statuses){
            return redirect()->route('system.order_statuses.index')->with('error', __('Sorry, we could not add the data'));
        }
        $order_statuses->delete();
        return redirect()->route('system.order_statuses.index')->with('delete',  __('Order Status deleted successfully'));
    }


}
