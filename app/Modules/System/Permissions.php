<?php

return [

    [
        'name' => __('Staff'),
        'description' => __('Staff Permissions'),
        'permissions' => [
            'view-all-staff'    =>['system.staff.index'],
            'view-one-staff'    =>['system.staff.show'],
            // 'delete-one-staff'  =>['system.staff.destroy'],
            'create-staff'      =>['system.staff.create','system.staff.store'],
            'update-staff'      =>['system.staff.edit','system.staff.update']
        ]
    ],
    [
        'name' => __('Drivers'),
        'description' => __('Driver Permissions'),
        'permissions' => [
            'view-all-driver'    =>['system.driver.index'],
            'view-one-driver'    =>['system.driver.show'],
             'delete-one-driver'  =>['system.driver.destroy'],
            'create-driver'      =>['system.driver.create','system.driver.store'],
            'update-driver'      =>['system.driver.edit','system.driver.update']
        ]
    ],    [
        'name' => __('Companies'),
        'description' => __('Companies Permissions'),
        'permissions' => [
            'view-all-company'    =>['system.company.index'],
            'view-one-company'    =>['system.company.show'],
             'delete-one-company'  =>['system.company.destroy'],
            'create-company'      =>['system.company.create','system.company.store'],
            'update-company'      =>['system.company.edit','system.company.update']
        ]
    ],
    [
        'name' => __('City'),
        'description' => __('City Permissions'),
        'permissions' => [
            'view-all-cities'    =>['system.cities.index'],
            'view-one-cities'    =>['system.cities.show'],
             'delete-one-cities'  =>['system.cities.destroy'],
            'create-cities'      =>['system.cities.create','system.cities.store'],
            'update-cities'      =>['system.cities.edit','system.cities.update']
        ]
    ],
    [
        'name' => __('District'),
        'description' => __('District Permissions'),
        'permissions' => [
            'view-all-districts'    =>['system.districts.index'],
            'view-one-districts'    =>['system.districts.show'],
             'delete-one-districts'  =>['system.districts.destroy'],
            'create-districts'      =>['system.districts.create','system.districts.store'],
            'update-districts'      =>['system.districts.edit','system.districts.update']
        ]
    ],
    [
        'name' => __('Orders'),
        'description' => __('Orders Permissions'),
        'permissions' => [
            'view-all-orders'    =>['system.orders.index'],
            'view-one-orders'    =>['system.orders.show'],
             'delete-one-orders'  =>['system.orders.destroy'],
            'create-orders'      =>['system.orders.create','system.orders.store'],
            'update-orders'      =>['system.orders.edit','system.orders.update']
        ]
    ],

    [
        'name' => __('Permission Group'),
        'description' => __('Permission Group Permissions'),
        'permissions' => [
            'view-all-permission-group'    =>['system.permission-group.index'],
            'view-one-permission-group'    =>['system.permission-group.show'],
            // 'delete-one-permission-group'  =>['system.permission-group.destroy'],
            'create-permission-group'      =>['system.permission-group.create','system.permission-group.store'],
            'update-permission-group'      =>['system.permission-group.edit','system.permission-group.update']
        ]
    ],

    [
        'name' => __('Setting'),
        'description' => __('Setting Permissions'),
        'permissions' => [
            'manage-setting'    =>['system.setting.index','system.setting.update']
        ]
    ],


    [
        'name' => __('Activity Log'),
        'description' => __('Activity Log'),
        'permissions' => [
            'view-activity-log'=>['system.activity-log.index'],
            'view-one-activity-log'=>['system.activity-log.show'],
        ]
    ],

    [
        'name' => __('Auth Sessions'),
        'description' => __('Auth Sessions'),
        'permissions' => [
            'view-auth-session'=>['system.staff.auth-sessions'],
            'delete-auth-session'=>['system.staff.delete-auth-sessions'],
        ]
    ],


    [
        'name' => __('Orders'),
        'permissions' => [
            'view-all-orders'    =>['system.orders.index'],
            'view-one-orders'    =>['system.orders.show'],
            'update-orders'      =>['system.orders.edit','system.orders.update']
        ]
    ],



];
