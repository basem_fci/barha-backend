<?php

namespace App\Modules\System;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Requests\CityFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class CityController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = City::select([
                'id',
                'name_'.clang().' as name',
            ]);
            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }


            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('action', function($data){
                    return $this->view('cities.action',compact('data'));
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Name'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'cities.id',
                'name'=>'cities.name',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('City')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add City'),
                'route'=>'system.cities.create'
            ];

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted City');
            }else{
                $this->viewData['pageTitle'] = __('City');
            }

            return $this->view('cities.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('City'),
            'url'=> route('system.cities.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create City'),
        ];

        $this->viewData['pageTitle'] = __('Create City');


        return $this->view('cities.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityFormRequest $request){
        $requestData = $request->all();

        $insertData = City::create($requestData);
        if($insertData){
            return redirect()->route('system.cities.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $cities
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request){

        $cities = City::find($id);
        if (!$cities){
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
        $this->viewData['breadcrumb'] = [
            [
                'text' => __('City'),
                'url' => route('system.cities.index'),
            ],
            [
                'text' => $cities->{'name_'.clang()},
            ]
        ];

        $this->viewData['pageTitle'] = __('City Profile');

        $this->viewData['result'] = $cities;
        return $this->view('cities.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $cities = City::find($id);
        if (!$cities){
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('City'),
            'url'=> route('system.cities.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $cities->{'name_'.clang()}]),
        ];

        $this->viewData['pageTitle'] = __('Edit City');
        $this->viewData['result'] = $cities;

        return $this->view('cities.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $cities
     * @return \Illuminate\Http\Response
     */
    public function update(CityFormRequest $request, $id)
    {
        $cities = City::find($id);
        if (!$cities){
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
        $requestData = $request->all();
        $updateData = $cities->update($requestData);
        if($updateData){
            return redirect()->route('system.cities.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $cities
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $cities = City::find($id);
        if (!$cities){
            return redirect()->route('system.cities.index')->with('error', __('Sorry, we could not add the data'));
        }
        $cities->delete();
        return redirect()->route('system.cities.index')->with('delete',  __('City deleted successfully'));
    }


}
