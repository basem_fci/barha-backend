<?php

namespace App\Modules\System;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceFormRequest;
use Form;
use Auth;
use Illuminate\Support\Str;
use Spatie\Activitylog\Models\Activity;
use Datatables;

class ServiceController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->isDataTable){

            $eloquentData = Service::select([
                'id',
                'image',
                'title_'.clang().' as title',
                'description_'.clang().' as description'
            ]);
            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('image', function($data){
                    return $this->view('service.image',compact('data'));
                })
                ->editColumn('title','{{$title}}')
                ->addColumn('description','{{ substr(strip_tags($description),0,50)}}')
                ->addColumn('action', function($data){
                    return $this->view('service.action',compact('data'));
                })
                ->escapeColumns(['description'])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Image'),
                __('Title'),
                __('Descreption'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'services.id',
                'image'=>'services.image',
                'title'=>'services.title',
                'description'=>'services.description',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Service')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Service'),
                'route'=>'system.service.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Service');
            }else{
                $this->viewData['pageTitle'] = __('Service');
            }


            return $this->view('service.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Service'),
            'url'=> route('system.service.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Service'),
        ];

        $this->viewData['pageTitle'] = __('Create Service');


        return $this->view('service.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceFormRequest $request){
        $requestData = $request->all();
        if($request->file('image')) {
            $requestData['image'] =UploadImage($request->file('image'), 'image');
        }
        $requestData['slug'] = Str::slug($request->title_ar);
        $insertData = Service::create($requestData);
        if($insertData){
            return redirect()->route('system.service.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.service.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service,Request $request){


            $this->viewData['breadcrumb'] = [
                [
                    'text' => __('Service'),
                    'url' => route('system.service.index'),
                ],
                [
                    'text' => $service->fullname,
                ]
            ];

            $this->viewData['pageTitle'] = __('Service Profile');


            $this->viewData['result'] = $service;
            return $this->view('service.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id ,Request $request){

        $service =Service::find($id);
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Service'),
            'url'=> route('system.service.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $service->{'title_'.clang()}]),
        ];

        $this->viewData['pageTitle'] = __('Edit Service');
        $this->viewData['result'] = $service;

        return $this->view('service.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceFormRequest $request, $id)
    {
        $service =Service::find($id);
        $requestData = $request->all();
        if($request->file('image')) {
            $requestData['image'] =UploadImage($request->file('image'), 'image',url('storage/app/'.$service->image));
        }
        $requestData['slug'] = Str::slug($request->title_ar);
        $updateData = $service->update($requestData);
        if ($updateData) {
            return redirect()->route('system.service.index')->with('updated', __('Data modified successfully'));
        } else {
            return redirect()->route('system.service.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect()->route('system.service.index')->with('delete',  __('Service deleted successfully'));

    }

}
