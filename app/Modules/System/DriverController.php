<?php

namespace App\Modules\System;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\DriverFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class DriverController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = User::select([
                'id',
                'name',
                'mobile',
                'status',
            ])->whereType('driver');
               // ->with('permission_group');

            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }

            whereBetween($eloquentData,'DATE(created_at)',$request->created_at1,$request->created_at2);

            if($request->id){
                $eloquentData->where('id','=',$request->id);
            }

            if($request->name){
                $eloquentData->where(function($query) use ($request){
                    $query->where('name','LIKE','%'.$request->name.'%');
                });
            }

            if($request->email){
                $eloquentData->where('email','LIKE','%'.$request->email.'%');
            }
            if($request->status){
                $eloquentData->where('status','LIKE','%'.$request->status.'%');
            }

            if($request->mobile){
                $eloquentData->where('mobile','LIKE','%'.$request->mobile.'%');
            }

            if ($request->downloadExcel == "true") {

                    $excelData = $eloquentData;
                    $excelData = $excelData->get();
                  return  exportXLS(__('Drivers'),
                        [
                            __('ID'),
                            __('Name'),
                            __('Mobile'),
                            __('Status'),
                        ],
                        $excelData,
                        [
                            'id' => 'id',
                            'name' => 'name',
                            'mobile' => 'mobile',
                            'status' => 'status'
                        ]
                    );
                }

            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('mobile', function($data){
                    return '<a href="tel:'.$data->mobile.'">'.$data->mobile.'</a>';
                })
                ->addColumn('status', function($data){
                    if($data->status == 'active'){
                        return '<span class="k-badge  k-badge--success k-badge--inline k-badge--pill">'.__('Active').'</span>';
                    }
                    return '<span class="k-badge  k-badge--danger k-badge--inline k-badge--pill">'.__('In-Active').'</span>';
                })
                ->addColumn('action', function($data){
                    return $this->view('driver.action',compact('data'));

                    return '<span class="dropdown">
                            <a href="#" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
                              <i class="la la-gear"></i>
                            </a>
                            <div class="dropdown-menu '.( (\App::getLocale() == 'ar') ? 'dropdown-menu-left' : 'dropdown-menu-right').'" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-36px, 25px, 0px);">
                                <a class="dropdown-item" href="'.route('system.driver.show',$data->id).'" target="_blank"><i class="far fa-eye"></i> '.__('View').'</a>
                                <a class="dropdown-item" href="'.route('system.driver.edit',$data->id).'"><i class="la la-edit"></i> '.__('Edit').'</a>
                                <a class="dropdown-item btn-danger" href="javascript:void(0);" onclick="deleteRecord(\''.route('system.driver.destroy',$data->id).'\')"><i class="la la-trash-o"></i> '.__('Delete').'</a> 
                            </div>
                        </span>';
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Name'),
                __('Mobile'),
                __('Status'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'users.id',
                'name'=>'users.name',
                'mobile'=>'users.mobile',
                'status'=>'users.status',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Drivers')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Driver'),
                'route'=>'system.driver.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Driver');
            }else{
                $this->viewData['pageTitle'] = __('Drivers');
            }

            return $this->view('driver.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Drivers'),
            'url'=> route('system.driver.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Driver'),
        ];

        $this->viewData['pageTitle'] = __('Create Driver');


        return $this->view('driver.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DriverFormRequest $request){
        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'picture');
        }
        if($request->file('picture_car_registration_form')) {
            $requestData['picture_car_registration_form'] = UploadImage($request->file('picture_car_registration_form'), 'picture_car_registration_form');
        }
        if($request->file('picture_resident_id')) {
            $requestData['picture_resident_id'] = UploadImage($request->file('picture_resident_id'), 'picture_resident_id');
        }
        if($request->file('picture_working_card_transport')) {
            $requestData['picture_working_card_transport'] = UploadImage($request->file('picture_working_card_transport'), 'picture_working_card_transport');
        }
        if($request->file('picture_driver_license')) {
            $requestData['picture_driver_license'] = UploadImage($request->file('picture_driver_license'), 'picture_driver_license');
        }
        $insertData = User::create($requestData);
        if($insertData){
            return redirect()->route('system.driver.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.driver.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(User $driver,Request $request){


            $this->viewData['breadcrumb'] = [
                [
                    'text' => __('Drivers'),
                    'url' => route('system.driver.index'),
                ],
                [
                    'text' => $driver->name,
                ]
            ];

            $this->viewData['pageTitle'] = __('Driver Profile');


            $this->viewData['result'] = $driver;
            return $this->view('driver.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(User $driver,Request $request){

        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Driver'),
            'url'=> route('system.driver.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $driver->name]),
        ];

        $this->viewData['pageTitle'] = __('Edit Driver');
        $this->viewData['result'] = $driver;

        return $this->view('driver.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(DriverFormRequest $request, User $driver)
    {
        $requestData = $request->all();

        if($requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'picture',url('storage/'.$driver->picture));
        }
        if($request->file('picture_car_registration_form')) {
            $requestData['picture_car_registration_form'] = UploadImage($request->file('picture_car_registration_form'), 'picture_car_registration_form',url('storage/'.$driver->picture_car_registration_form));
        }
        if($request->file('picture_resident_id')) {
            $requestData['picture_resident_id'] = UploadImage($request->file('picture_resident_id'), 'picture_resident_id', url('storage/'.$driver->picture_resident_id));
        }
        if($request->file('picture_working_card_transport')) {
            $requestData['picture_working_card_transport'] = UploadImage($request->file('picture_working_card_transport'), 'picture_working_card_transport',url('storage/'.$driver->picture_working_card_transport));
        }
        if($request->file('picture_driver_license')) {
            $requestData['picture_driver_license'] = UploadImage($request->file('picture_driver_license'), 'picture_driver_license',url('storage/'.$driver->picture_driver_license));
        }
        $updateData = $driver->update($requestData);

        if($updateData){
            return redirect()->route('system.driver.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.driver.index')->with('error', __('Sorry, we could not add the data'));
        }
    }


    public function changePassword(){

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Change Password'),
        ];
        $this->viewData['pageTitle'] = __('Change Password');

        return $this->view('driver.change-password',$this->viewData);

    }

    public function changePasswordPost(DriverFormRequest $request){

        if(!\Hash::check($request->currant_password, Auth::driver()->password)){
            return $this->response(
                false,
                11001,
                __('Wrong Currant Password')
            );
        }elseif($request->currant_password == $request->password){
            return $this->response(
                false,
                11001,
                __('New password can\'t be currant password')
            );
        }


        $insertData = User::where('id',Auth::id())
            ->update([
                'password'=> bcrypt($request->password)
            ]);

        if($insertData){
            return $this->response(
                true,
                200,
                __('password updated successfully'),
                [
                    'url'=> route('system.dashboard')
                ]
            );
        }else{
            return $this->response(
                false,
                11001,
                __('Sorry, we could not update data')
            );
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $driver,Request $request)
    {
        $driver->delete();
        return redirect()->route('system.driver.index')->with('delete',  __('Driver deleted successfully'));
    }

    /**
     * Active - not active driver
     * @param  \App\Driver  $driver
     */
    public function Status($id)
    {
        $driver = User::find($id);
        if ($driver->status == 'active') {
            $driver->status = 'in-active';
        } else {
            $driver->status = 'active';
        }
        if ($driver->save()) {
            return redirect()->route('system.driver.index')->with('updated', __('Driver modified successfully'));

        }
        return redirect()->route('system.driver.index')->with('error', __('Sorry, we could not add the data'));
    }


}
