<?php

namespace App\Modules\System;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyFormRequest;
use Form;
use Auth;
use Datatables;
use File;
class CompanyController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = User::select([
                'id',
                'name',
                'mobile',
                'download_manager_number',
                'logistic_manager_number',
                'status',
            ])->whereType('company');

            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }

            whereBetween($eloquentData,'DATE(created_at)',$request->created_at1,$request->created_at2);

            if($request->id){
                $eloquentData->where('id','=',$request->id);
            }
            if($request->name){
                $eloquentData->where(function($query) use ($request){
                    $query->where('name','LIKE','%'.$request->name.'%');
                });
            }
            if($request->status){
                $eloquentData->where('status','LIKE','%'.$request->status.'%');
            }
            if($request->mobile){
                $eloquentData->where('mobile','LIKE','%'.$request->mobile.'%');
            }
            if($request->logistic_manager_number){
                $eloquentData->where('logistic_manager_number','LIKE','%'.$request->logistic_manager_number.'%');
            }
            if($request->download_manager_number){
                $eloquentData->where('download_manager_number','LIKE','%'.$request->download_manager_number.'%');
            }

            if ($request->downloadExcel == "true") {

                    $excelData = $eloquentData;
                    $excelData = $excelData->get();
                  return  exportXLS(__('Company'),
                        [
                            __('ID'),
                            __('Company Name'),
                            __('Mobile Company'),
                            __('Mobile Download Manager'),
                            __('Mobile Logistic Manager'),
                            __('Status'),
                        ],
                        $excelData,
                        [
                            'id' => 'id',
                            'name' => 'name',
                            'mobile' => 'mobile',
                            'download_manager_number' => 'download_manager_number',
                            'logistic_manager_number' => 'logistic_manager_number',
                            'status' => 'status'
                        ]
                    );
                }

            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('mobile', function($data){
                    return '<a href="tel:'.$data->mobile.'">'.$data->mobile.'</a>';
                })
                ->addColumn('download_manager_number', function($data){
                    return '<a href="tel:'.$data->mobile.'">'.$data->download_manager_number.'</a>';
                })
                ->addColumn('logistic_manager_number', function($data){
                    return '<a href="tel:'.$data->mobile.'">'.$data->logistic_manager_number.'</a>';
                })
                ->addColumn('status', function($data){
                    if($data->status == 'active'){
                        return '<span class="k-badge  k-badge--success k-badge--inline k-badge--pill">'.__('Active').'</span>';
                    }
                    return '<span class="k-badge  k-badge--danger k-badge--inline k-badge--pill">'.__('In-Active').'</span>';
                })
                ->addColumn('action', function($data){
                    return $this->view('company.action',compact('data'));

                    return '<span class="dropdown">
                            <a href="#" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
                              <i class="la la-gear"></i>
                            </a>
                            <div class="dropdown-menu '.( (\App::getLocale() == 'ar') ? 'dropdown-menu-left' : 'dropdown-menu-right').'" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-36px, 25px, 0px);">
                                <a class="dropdown-item" href="'.route('system.company.show',$data->id).'" target="_blank"><i class="far fa-eye"></i> '.__('View').'</a>
                                <a class="dropdown-item" href="'.route('system.company.edit',$data->id).'"><i class="la la-edit"></i> '.__('Edit').'</a>
                                <a class="dropdown-item btn-danger" href="javascript:void(0);" onclick="deleteRecord(\''.route('system.company.destroy',$data->id).'\')"><i class="la la-trash-o"></i> '.__('Delete').'</a> 
                            </div>
                        </span>';
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Company Name'),
                __('Mobile Company'),
                __('Mobile Download Manager'),
                __('Mobile Logistic Manager'),
                __('Status'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'users.id',
                'name'=>'users.name',
                'mobile'=>'users.mobile',
                'download_manager_number'=>'users.download_manager_number',
                'logistic_manager_number'=>'users.logistic_manager_number',
                'status'=>'users.status',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Companies')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Company'),
                'route'=>'system.company.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Company');
            }else{
                $this->viewData['pageTitle'] = __('Company');
            }

            return $this->view('company.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Companies'),
            'url'=> route('system.company.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Company'),
        ];

        $this->viewData['pageTitle'] = __('Create Company');


        return $this->view('company.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyFormRequest $request){

        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'picture');
        }

        $insertData = User::create($requestData);
        if($insertData){
            return redirect()->route('system.company.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.company.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(User $company,Request $request){


            $this->viewData['breadcrumb'] = [
                [
                    'text' => __('Company'),
                    'url' => route('system.company.index'),
                ],
                [
                    'text' => $company->name,
                ]
            ];

            $this->viewData['pageTitle'] = __('Company Profile');


            $this->viewData['result'] = $company;
            return $this->view('company.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(User $company,Request $request){

        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Companies'),
            'url'=> route('system.company.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $company->name]),
        ];

        $this->viewData['pageTitle'] = __('Edit Company');
        $this->viewData['result'] = $company;

        return $this->view('company.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyFormRequest $request, User $company)
    {
        $requestData = $request->all();

        if($requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'picture',url('storage/'.$company->picture));
        }
        if($request->file('picture_car_registration_form')) {
            $requestData['picture_car_registration_form'] = UploadImage($request->file('picture_car_registration_form'), 'picture_car_registration_form',url('storage/'.$company->picture_car_registration_form));
        }
        if($request->file('picture_resident_id')) {
            $requestData['picture_resident_id'] = UploadImage($request->file('picture_resident_id'), 'picture_resident_id', url('storage/'.$company->picture_resident_id));
        }
        if($request->file('picture_working_card_transport')) {
            $requestData['picture_working_card_transport'] = UploadImage($request->file('picture_working_card_transport'), 'picture_working_card_transport',url('storage/'.$company->picture_working_card_transport));
        }
        if($request->file('picture_company_license')) {
            $requestData['picture_company_license'] = UploadImage($request->file('picture_company_license'), 'picture_company_license',url('storage/'.$company->picture_company_license));
        }
        $updateData = $company->update($requestData);
        if($updateData){
            return redirect()->route('system.company.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.company.index')->with('error', __('Sorry, we could not add the data'));
        }
    }


    public function changePassword(){

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Change Password'),
        ];
        $this->viewData['pageTitle'] = __('Change Password');

        return $this->view('company.change-password',$this->viewData);

    }

    public function changePasswordPost(CompanyFormRequest $request){

        if(!\Hash::check($request->currant_password, Auth::company()->password)){
            return $this->response(
                false,
                11001,
                __('Wrong Currant Password')
            );
        }elseif($request->currant_password == $request->password){
            return $this->response(
                false,
                11001,
                __('New password can\'t be currant password')
            );
        }


        $insertData = User::where('id',Auth::id())
            ->update([
                'password'=> bcrypt($request->password)
            ]);

        if($insertData){
            return $this->response(
                true,
                200,
                __('password updated successfully'),
                [
                    'url'=> route('system.dashboard')
                ]
            );
        }else{
            return $this->response(
                false,
                11001,
                __('Sorry, we could not update data')
            );
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $company,Request $request)
    {
        $company->delete();
        return redirect()->route('system.company.index')->with('delete',  __('Company deleted successfully'));
    }


}
