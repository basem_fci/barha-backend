<?php

namespace App\Modules\System;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Requests\SliderFormRequest;
use Form;
use Auth;
use Spatie\Activitylog\Models\Activity;
use Datatables;

class SliderController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = Slider::select([
                'id',
                'image'
            ]);
            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('image', function($data){
                    return $this->view('slider.image',compact('data'));
                })
                ->addColumn('action', function($data){
                    return $this->view('slider.action',compact('data'));
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Image'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'sliders.id',
                'image'=>'sliders.image',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('Slider')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add Slider'),
                'route'=>'system.slider.create'
            ];

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted Slider');
            }else{
                $this->viewData['pageTitle'] = __('Slider');
            }


            return $this->view('slider.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Slider'),
            'url'=> route('system.slider.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create Slider'),
        ];

        $this->viewData['pageTitle'] = __('Create Slider');


        return $this->view('slider.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderFormRequest $request){
        $requestData = $request->all();
        if($request->file('avatar')){

            $path = $request->file('avatar')->store(setting('system_path').'/avatar/'.date('Y/m/d'));
            if($path){
                $requestData['image'] = $path;
            }
        }else{
            unset($requestData['image']);
        }
        $insertData = Slider::create($requestData);
        if($insertData){
            return redirect()->route('system.slider.index')->with('add',  __('Data added successfully'));
        }else{
            return redirect()->route('system.slider.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider,Request $request){


            $this->viewData['breadcrumb'] = [
                [
                    'text' => __('Slider'),
                    'url' => route('system.slider.index'),
                ],
                [
                    'text' => $slider->fullname,
                ]
            ];

            $this->viewData['pageTitle'] = __('Slider Profile');


            $this->viewData['result'] = $slider;
            return $this->view('slider.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,Request $request){

        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('Slider'),
            'url'=> route('system.slider.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit'),
        ];

        $this->viewData['pageTitle'] = __('Edit Slider');
        $this->viewData['result'] = $slider;

        return $this->view('slider.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(SliderFormRequest $request, Slider $slider)
    {
        $requestData = $request->all();
        if($request->file('avatar')){
            $path = $request->file('avatar')->store(setting('system_path').'/avatar/'.date('Y/m/d'));
            if($path){
                $requestData['avatar'] = $path;
            }
        }else{
            unset($requestData['avatar']);
        }

        $updateData = $slider->update($requestData);
        if($updateData){
            return redirect()->route('system.slider.index')->with('updated',  __('Data modified successfully'));
        }else{
            return redirect()->route('system.slider.index')->with('error', __('Sorry, we could not add the data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
        return redirect()->route('system.slider.index')->with('delete',  __('Slider deleted successfully'));

    }

}
