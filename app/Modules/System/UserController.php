<?php

namespace App\Modules\System;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserFormRequest;
use Form;
use Auth;
use Datatables;

class UserController extends SystemController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->isDataTable){

            $eloquentData = User::select([
                'id',
                'name',
                'mobile',
                'status',
            ]);
               // ->with('permission_group');

            if($request->withTrashed){
                $eloquentData->onlyTrashed();
            }


            whereBetween($eloquentData,'DATE(created_at)',$request->created_at1,$request->created_at2);

            if($request->id){
                $eloquentData->where('id','=',$request->id);
            }

            if($request->name){
                $eloquentData->where(function($query) use ($request){
                    $query->where('name','LIKE','%'.$request->name.'%');
                });
            }

            if($request->email){
                $eloquentData->where('email','LIKE','%'.$request->email.'%');
            }

            if($request->mobile){
                $eloquentData->where('mobile','LIKE','%'.$request->mobile.'%');
            }

            if($request->permission_group_id){
                $eloquentData->where('permission_group_id','=',$request->permission_group_id);
            }

            if ($request->downloadExcel == "true") {

                    $excelData = $eloquentData;
                    $excelData = $excelData->get();
                  return  exportXLS(__('User'),
                        [
                            __('ID'),
                            __('Name'),
                            __('Mobile'),
                            __('Permission Group'),
                            __('Status'),
                        ],
                        $excelData,
                        [
                            'id' => 'id',
                            'name' => 'name',
                            'mobile' => 'mobile',
                            'permission_group' => function($data){
                                return $data->permission_group->name;
                            },
                            'status' => 'status'
                        ]
                    );
                }





            return Datatables::of($eloquentData)
                ->addColumn('id','{{$id}}')
                ->addColumn('name', function($data){
                    return $data->name;
                })
                ->addColumn('mobile', function($data){
                    return '<a href="tel:'.$data->mobile.'">'.$data->mobile.'</a>';
                })
                ->addColumn('permission_group_id', function($data){
                    return $data->permission_group->name;
                })
                ->addColumn('status', function($data){
                    if($data->status == 'active'){
                        return '<span class="k-badge  k-badge--success k-badge--inline k-badge--pill">'.__('Active').'</span>';
                    }
                    return '<span class="k-badge  k-badge--danger k-badge--inline k-badge--pill">'.__('In-Active').'</span>';
                })
                ->addColumn('action', function($data){
                    return '<span class="dropdown">
                            <a href="#" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
                              <i class="la la-gear"></i>
                            </a>
                            <div class="dropdown-menu '.( (\App::getLocale() == 'ar') ? 'dropdown-menu-left' : 'dropdown-menu-right').'" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-36px, 25px, 0px);">
                                <a class="dropdown-item" href="'.route('system.user.show',$data->id).'" target="_blank"><i class="far fa-eye"></i> '.__('View').'</a>
                                <a class="dropdown-item" href="'.route('system.user.edit',$data->id).'"><i class="la la-edit"></i> '.__('Edit').'</a>
                               <!--  <a class="dropdown-item" href="javascript:void(0);" onclick="deleteRecord(\''.route('system.user.destroy',$data->id).'\')"><i class="la la-trash-o"></i> '.__('Delete').'</a> -->
                            </div>
                        </span>';
                })
                ->escapeColumns([])
                ->make(true);
        }
        else{
            // View Data
            $this->viewData['tableColumns'] = [
                __('ID'),
                __('Name'),
                __('Mobile'),
                __('Permission Group'),
                __('Status'),
                __('Action')
            ];

            $this->viewData['js_columns'] =[
                'id'=>'user.id',
                'name'=>'user.name',
                'mobile'=>'user.mobile',
                'permission_group_id'=>'user.permission_group_id',
                'status'=>'user.status',
                'action'=>'action'
            ];

            $this->viewData['breadcrumb'][] = [
                'text'=> __('User')
            ];

            $this->viewData['add_new'] = [
                'text'=> __('Add User'),
                'route'=>'system.user.create'
            ];
            $this->viewData['filter'] = true;
            $this->viewData['download_excel'] = true;

            if($request->withTrashed){
                $this->viewData['pageTitle'] = __('Deleted User');
            }else{
                $this->viewData['pageTitle'] = __('User');
            }


            $this->viewData['PermissionGroup'] = array_column(PermissionGroup::get()->toArray(),'name','id');

            return $this->view('user.index',$this->viewData);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('User'),
            'url'=> route('system.user.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Create User'),
        ];

        $this->viewData['pageTitle'] = __('Create User');

        $this->viewData['PermissionGroup'] = PermissionGroup::get();

        return $this->view('user.create',$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request){
        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        $insertData = User::create($requestData);
        if($insertData){
            return $this->response(
                true,
                200,
                __('Data added successfully'),
                [
                    'url'=> route('system.user.show',$insertData->id)
                ]
            );
        }else{
            return $this->response(
                false,
                11001,
                __('Sorry, we could not add the data')
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user,Request $request){


            $this->viewData['breadcrumb'] = [
                [
                    'text' => __('User'),
                    'url' => route('system.user.index'),
                ],
                [
                    'text' => $user->fullname,
                ]
            ];

            $this->viewData['pageTitle'] = __('User Profile');


            $this->viewData['result'] = $user;
            return $this->view('user.show', $this->viewData);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user,Request $request){

        // Main View Vars
        $this->viewData['breadcrumb'][] = [
            'text'=> __('User'),
            'url'=> route('system.user.index')
        ];

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Edit (:name)',['name'=> $user->fullname]),
        ];

        $this->viewData['pageTitle'] = __('Edit User');
        $this->viewData['result'] = $user;
        $this->viewData['PermissionGroup'] = PermissionGroup::get();

        return $this->view('user.create',$this->viewData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request, User $user)
    {

        $requestData = $request->all();

        if($requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }

        if($request->file('avatar')){
            $path = $request->file('avatar')->store(setting('system_path').'/avatar/'.date('Y/m/d'),'first_public');
            if($path){
                $requestData['avatar'] = $path;
            }
        }else{
            unset($requestData['avatar']);
        }

        $updateData = $user->update($requestData);

        if($updateData){
            return $this->response(
                true,
                200,
                __('Data modified successfully'),
                [
                    'url'=> route('system.user.show',$user->id)
                ]
            );
        }else{
            return $this->response(
                false,
                11001,
                __('Sorry, we could not edit the data')
            );
        }
    }


    public function changePassword(){

        $this->viewData['breadcrumb'][] = [
            'text'=> __('Change Password'),
        ];
        $this->viewData['pageTitle'] = __('Change Password');

        return $this->view('user.change-password',$this->viewData);

    }

    public function changePasswordPost(UserFormRequest $request){

        if(!\Hash::check($request->currant_password, Auth::user()->password)){
            return $this->response(
                false,
                11001,
                __('Wrong Currant Password')
            );
        }elseif($request->currant_password == $request->password){
            return $this->response(
                false,
                11001,
                __('New password can\'t be currant password')
            );
        }


        $insertData = User::where('id',Auth::id())
            ->update([
                'password'=> bcrypt($request->password)
            ]);

        if($insertData){
            return $this->response(
                true,
                200,
                __('password updated successfully'),
                [
                    'url'=> route('system.dashboard')
                ]
            );
        }else{
            return $this->response(
                false,
                11001,
                __('Sorry, we could not update data')
            );
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user,Request $request)
    {
        $message = __('User deleted successfully');
        $user->delete();
        return $this->response(true,200,$message);
    }

}
