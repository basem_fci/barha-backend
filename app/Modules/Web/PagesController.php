<?php

namespace App\Modules\Web;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Project;
use App\Models\Service;


class PagesController extends Controller{

    protected $viewData = [];

    public function About(){
        return view('web.pages.about-us');
    }

    public function services(){
        $service = Service::paginate(12);
        return view('web.pages.service',compact('service'));
    }


    public function privacyPolicy(){
        return view('web.pages.privacy-policy');
    }


    public function serviceDetails($slug){
        $service = Service::whereSlug($slug)->first();
        return view('web.pages.service-details',compact('service'));
    }

    public function TermsConditions(){
        return view('web.pages.terms-conditions');
    }

    public function ContactUs(){
        return view('web.pages.contact-us');
    }

    public function ContactUsPOst(Request $request){
        $contact = new ContactUs();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $contact->save();
        return redirect()->back()->with('add',  __('Data added successfully'));
    }


}