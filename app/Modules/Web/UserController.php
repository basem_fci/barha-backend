<?php

namespace App\Modules\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\models\City;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserController extends Controller{

    public function __construct()
    {
        $this->middleware( ['auth'] );
    }

    public function Dashboard(){
        if (Auth::user()->type =='driver')
        {
            return view('web.profile.driver-profile');

        }
        $city =  DB::table('cities')->get();

        return view('web.profile.profile',compact('city'));
    }

}