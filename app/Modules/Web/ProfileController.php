<?php

namespace App\Modules\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyFormRequest;
use App\Http\Requests\DriverFormRequest;
use App\Http\Requests\UserFormRequest;
use App\Models\CategoryProject;
use App\Models\Project;
use App\Models\ProjectAttatchment;
use App\Models\ProjectComment;
use App\Models\ProjectTask;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Psy\Util\Str;


class ProfileController extends Controller{

    public function __construct()
    {
//        $this->middleware( ['auth'] )->except('Login','loginPOst','Register','RegisterPost','ResetPassword','ResetPasswordPost');
    }

    public function Login(){
        $user = Auth::user();
        if ($user){
            return redirect()->route('web.index');
        }
        return view('web.profile.login');
    }

    public function loginPOst(){
        if(Auth::attempt(['mobile' => preparePhone(request('mobile')), 'password' => request('password')])){
            $status = Auth::user()->status;
            if ($status == 'in-active'){
                $this->guard()->logout();
                return redirect()->back()->with('error',__('Login Field Account Not Active'));
            }
            return redirect()->route('web.dashboard')->with('success',__('Login successfully'));
        }
        else{
            return redirect()->back()->with('error',__('Login Fail Mobile or password error'));
        }
    }

    public function showForgetPasswordForm(){
        $user = Auth::user();
        if ($user){
            return redirect()->route('web.index');
        }
        return view('web.profile.forget-password');
    }

    public function submitForgetPasswordForm(Request $request){
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token =generateRandomString(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $user = User::where('email', $request->email)->first();
        $logo = '<img src='.'"'.url('assets/front/images/logo-h.png').">";
        Mail::send('web.email.forget-password', ['token' => $token,'logo' => $logo,'name' => $user->name??''], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });

        return back()->with('success', 'We have e-mailed your password reset link!');
    }

    public function showResetPasswordForm($token) {
        return view('web.profile.forget-password-link',['token' => $token]);
    }

    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])
            ->first();

        if(!$updatePassword){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return redirect('/login')->with('success', 'Your password has been changed!');
    }

    public function Register(){
        $user = Auth::user();
        if ($user){
            return redirect()->route('web.index');
        }
        return view('web.profile.register');
    }

    public function RegisterDriverPost(DriverFormRequest $request){
        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'picture');
        }
        if($request->file('picture_car_registration_form')) {
            $requestData['picture_car_registration_form'] = UploadImage($request->file('picture_car_registration_form'), 'picture_car_registration_form');
        }
        if($request->file('picture_resident_id')) {
            $requestData['picture_resident_id'] = UploadImage($request->file('picture_resident_id'), 'picture_resident_id');
        }
        if($request->file('picture_working_card_transport')) {
            $requestData['picture_working_card_transport'] = UploadImage($request->file('picture_working_card_transport'), 'picture_working_card_transport');
        }
        if($request->file('picture_driver_license')) {
            $requestData['picture_driver_license'] = UploadImage($request->file('picture_driver_license'), 'picture_driver_license');
        }
        $requestData['mobile'] = preparePhone($requestData['mobile']);
        if ($request->nationality == 'saudi'){
            $requestData['status'] = 'in-active';
        }else{
            $requestData['status'] = 'active';
        }
        $insertData = User::create($requestData);
        if($insertData){
            return redirect()->route('web.register')->with('success','Profile Created successfully');
        }else{
            return redirect()->back()->with('error', __('Sorry, we could not add the data'));
        }
    }

    public function EditProfile(){
        return view('web.profile.edit-profile');
    }

    public function RegisterCompanyPost(CompanyFormRequest $request)
    {
        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        if ($request->file('picture')) {
            $requestData['picture'] = UploadImage($request->file('picture'), 'picture');
        }
        $requestData['mobile'] = preparePhone($requestData['mobile']);

        $insertData = User::create($requestData);
        if($insertData){
            return redirect()->route('web.register')->with('success','Profile Created successfully');
        }
        return redirect()->back()->with('error', __('Sorry, we could not add the data'));
    }


    public function UpdateProfile(UserFormRequest $request,$id){
        $user=User::find($id);
        $requestData = $request->all();
        if($request->file('picture')) {
            $requestData['picture'] =UploadImage($request->file('picture'), 'profile-picture',url('storage/'.$user->image));
        }
        $updateData = $user->update($requestData);
        if($updateData){
            return redirect()->route('web.dashboard')->with('updated',  __('Profile Updated successfully'));
        }else{
            return redirect()->back()->with('error', __('Sorry, we could not add the data'));
        }
    }

    public function ChangePassword(UserFormRequest $request){

        if(!\Hash::check($request->currant_password, Auth::user()->password)){
            return redirect()->back()->with('error', __('Wrong Currant Password'));
        }elseif($request->currant_password == $request->password){
            return redirect()->back()->with('error', __('New password can\'t be currant password'));
        }
        $updateData = User::where('id',Auth::id())
            ->update([
                'password'=> bcrypt($request->password)
            ]);

        if($updateData){
            return redirect()->route('web.dashboard')->with('updated',  __('password Updated successfully'));
        }else{
            return redirect()->back()->with('error', __('Sorry, we could not add the data'));
        }
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('web.index')->with('success',__('Logout successfully'));
    }

    protected function guard()
    {
        return Auth::guard('web');
    }
}