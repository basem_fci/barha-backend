<?php

namespace App\Modules\Web;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\models\City;
use App\Models\ContactUs;
use App\Models\Order;
use App\Models\Project;
use App\Models\Service;
use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WebController extends Controller{

    protected $viewData = [];


    public function index(){
        return view('web.index',
            [
                'slider'=>Slider::all(),
                'service'=>Service::OrderBy('id','DESC')->take(3)->get(),
                'featured_order'=>$this->FeaturedOrder(),
                'last_order'=>$this->LatestOrder(),
            ]
        );
    }

    private function FeaturedOrder(){
        return Order::
//            whereDate('created_at', Carbon::today())->
            where('order_status_id',1)->OrderBy('id','DESC')->take(5)->get();
    }

    private function LatestOrder(){
        return Order::where('order_status_id',1)
            ->whereNotIn('id', $this->FeaturedOrder()->pluck('id'))
            ->OrderBy('id','DESC')->take(6)->get();
    }
    public function About(){
        $service = Service::get();
        return view('web.about',compact('service'));
    }

    public function Members(){

        return view('web.members');
    }
        public function upload_media(Request $request){

        $input = $request->only(['media_type', 'media']);
        $validator = Validator::make($input, [
            'media_type' => 'required|string|in:sound,image',
            'media' => 'required|mimes:jpeg,jpg,png,mp3,amr,wav'
        ]);

        if ($validator->fails()) {
            return $this->fail('Validation Error.', $validator->errors());
        }

        if(!$request->file('media')){
            return $this->fail(__('media is required'));
        }


        if($request->file('media')){
            $mediaName = time().'.'.$request->media->extension();
            $path = 'media/'.$input['media_type'].'/'.$mediaName;
            $request->media->move(public_path('media/'.$input['media_type']), $mediaName);

            if($path){
                $input['media'] = $path;
                return $this->success(__('file uploaded'),['path'=>asset($path)]);

            }else{
                return $this->fail(__('Please try again later'));
            }
        }

    }


}