<?php

namespace App\Modules\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderFormRequest;
use App\Models\AuthSession;
use App\models\District;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class OrderController extends Controller{

    public function __construct()
    {
        $this->middleware( ['auth'] )->except('language');
    }


    public function orders (){
        $orders = Order::whereIn('order_status_id',[1,7])->orderBy('id','DESC')->paginate(12);
        if (!$orders){
            return redirect()->back()->with('error','Page Not Found');
        }
        return view('web.orders.orders',compact('orders'));
    }
    public function storeOrders(OrderFormRequest $request){
        $requestData = $request->all();
        $requestData['total_price'] = (float)$request->price+(float)setting('value_added')+(float)setting('shipping_charges');
        $insertData = Order::create($requestData);
        if ($insertData->record(1,'Order Created successfully','App\Models\User')) {
            return redirect()->back()->with('add',  __('Order added successfully'));
        }else{
            return redirect()->back()->with('error', __('Sorry, we could not add the data'));
        }
    }


    public function updateOrders(OrderFormRequest $request,$id)
    {
        $orders = Order::find($id);
        $requestData = $request->all();
        $requestData['total_price'] = (float)$request->price+(float)setting('value_added')+(float)setting('shipping_charges');
        $updateData = $orders->update($requestData);
        if ($updateData->record(1,'Order Created successfully','App\Models\User')) {
            return redirect()->back()->with('updated',  __('Order modified successfully'));
        }else{
            return redirect()->back()->with('error', __('Sorry, we could not add the data'));
        }
    }


    public function orderDetails($tracking_number){

        $order = Order::whereTrackingNumber($tracking_number);
        if (Auth::user()->type =='company'){
            $order=$order->where('creatable_id',Auth::id())->where('creatable_type','App\Models\User');
        }
        $order=$order->first();
        if (!$order){
            return redirect()->back()->with('error','Page Not Found');
        }
        return view('web.orders.order-details',compact('order'));
    }

    public function tracking($tracking_number){
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order){
            return redirect()->back()->with('error','Page Not Found');
        }
        if (Auth::user()->type == 'driver'){
            return view('web.orders.order-tracking-driver',compact('order'));
        }
        return view('web.orders.order-tracking',compact('order'));
    }

    public function updateLocation(Request $request){
        $user = User::find(Auth::id());
        $user->lat = $request->lat??'';
        $user->lng = $request->lng??'';
        $user->save();
        return $user;
    }

    public function confirmOrder($tracking_number)
    {
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order->order_status_id = 4;
        if ($order->save()) {
            $order->record($order->order_status_id, "Driver Way To Delivered Order", "App\Models\User");
            return redirect()->back()->with('success', __('Driver Order successfully!'));
        }
        return redirect()->back()->with('error', __('Fail Send Offer'));
    }



    public function deliveredOrder($tracking_number)
    {
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order->order_status_id = 5;
        if ($order->save()) {
            $order->record($order->order_status_id, "Driver Delivered Order", "App\Models\User");
            return redirect()->back()->with('success', __('Driver Order successfully!'));
        }
        return redirect()->back()->with('error', __('Fail Send Offer'));
    }

    public function confirmDeliveredOrder($tracking_number)
    {
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order->order_status_id = 9;
        if ($order->save()) {
            $order->record($order->order_status_id, "Company Confirm Delivered Order", "App\Models\User");
            return redirect()->back()->with('success', __('Company Confirm Order successfully!'));
        }
        return redirect()->back()->with('error', __('Fail Send Offer'));
    }

    public function returnOrder($tracking_number)
    {
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order->order_status_id = 6;
        if ($order->save()) {
            $order->record($order->order_status_id, "Driver Return Order", "App\Models\User");
            return redirect()->back()->with('success', __('Return Order successfully!'));
        }
        return redirect()->back()->with('error', __('Fail Send Offer'));
    }

    public function reshipOrder($tracking_number)
    {
        $order = Order::whereTrackingNumber($tracking_number)->first();
        if (!$order) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order->order_status_id = 7;
        if ($order->save()) {
            $order->record($order->order_status_id, "Company Reship Order", "App\Models\User");
            return redirect()->back()->with('success', __('Reship Order successfully!'));
        }
        return redirect()->back()->with('error', __('Fail Send Offer'));
    }

    public function addOffer(Request $request){
        $order = Order::whereIn('order_status_id',[1,7])->whereId($request->order_id)->first();
        if (!$order){
            return redirect()->back()->with('error',__('You cant add offer to this order try with another order'));
        }

        $order->order_status_id  = 2;
        $order->driver_id  = Auth::id();
        $order->notes = $request->notes;
        if ($order->save()){
            $order->record( $order->order_status_id,$request->notes ,"App\Models\User");
            $order->addOffer( $order->order_status_id,$request->notes );
            return redirect()->back()->with('success', __('Your Offer Send successfully!'));
        }
        return redirect()->back()->with('error',__('Fail Send Offer'));
    }

    public function acceptOffer($id){
        $order_offer = OrderOffer::find($id);
        if (!$order_offer){
            return redirect()->back()->with('error','Page Not Found');
        }
        $order_offer->order_status_id  = 3;
        if ($order_offer->save()) {
            $order = Order::find($order_offer->order_id);
            $order->order_status_id = 3;
            $order->notes = null;
            if ($order->save()) {
                $order = Order::find($order_offer->order_id);
                $order->record($order->order_status_id, 'Accept Offer successfully', "App\Models\User");
                return redirect()->back()->with('success', __('Accept Offer successfully'));
            }
        }
        return redirect()->back()->with('error','Page Not Found');
    }
    public function cancelOffer($id)
    {
        $order_offer = OrderOffer::find($id);
        if (!$order_offer) {
            return redirect()->back()->with('error', 'Page Not Found');
        }
        $order_offer->order_status_id = 8;
        $order_offer->comment = 'Cancel Offer';
        if ($order_offer->save()) {
            $order = Order::find($order_offer->order_id);
            $order->order_status_id = 1;
            $order->driver_id = null;
            $order->notes = null;
            $order->save();
            $order->record($order->order_status_id, 'Cancel Offer successfully', "App\Models\User");
            return redirect()->back()->with('success', __('Cancel Offer successfully!'));
        }
        return redirect()->back()->with('error', 'Page Not Found');
    }
    public function index(Request $request){

        switch ($request->type) {


            case 'available_driver':
                $drivers = User::where('id',$request->driver_id)->get();
                $check_online = AuthSession::
                where('id',$request->driver_id)
                    ->where('guard_name','user')->pluck('user_id');
                return  $drivers;

                break;

            case 'all_available_driver':
                $online_driver_from_app = AuthApi::where('guard_name','driver_api')->pluck('user_id');
                if($online_driver_from_app->IsEmpty()){
                    return [];
                }else{
                    $driver_ids = $online_driver_from_app->toArray();
                    $online_driver_from_web = App\Models\AuthSession::where('guard_name','driver')->pluck('user_id');
                    if($online_driver_from_web->isNotEmpty()){
                        $driver_ids =   array_merge($online_driver_from_web->toArray(),$driver_ids);
                    }
                    $drivers = Driver::select('id','name','lat','lng','available','plan_id',
                        \DB::raw('(CASE WHEN id in ('.setting("escape_drivers_from_auto_assign").') THEN "yes" ELSE "no" END)  as escape')
                    )
                        ->whereIn('id',$driver_ids)
                        -> withCount(['orders'=>function($q){
                            return  $q->whereIn('status',['accepted','arrived','collected']);
                        }])->get();
                    return  $drivers;
                }
                break;
            case 'saveLog':
                if (!empty($request->desc) && !empty($request->model) && !empty($request->id)) {
                    save_log(__($request->desc), $request->model, $request->id);
                    return [];
                }
                return [];
                break;

            case 'readNotification':
                foreach (Auth::user()->unreadNotifications as $notification) {
                    $notification->markAsRead();
                }
                break;

            case 'getNextAreas':
                return AreasData::getNextAreas($request->id);
                break;


            case 'dropdownMenuArea':
                $id = $request->id;


                if ($id == 0) {
                    return [
                        'area_type_id' => 1,
                        'areas' => Area::where('area_type_id', 1)->get(['id', 'name_' . \App::getLocale() . ' as name'])
                    ];
                }

                $data = AreasData::getNextAreas($id);

                $returnData = [];
                if (!empty($data['areas'])) {
                    foreach ($data['areas'] as $key => $value) {
                        $returnData[] = [
                            'id' => $value['id'],
                            'name' => $value['name_' . \App::getLocale()]
                        ];
                    }

                    return [
                        'area_type_id' => $data['type']->id,
                        'areas' => $returnData
                    ];
                }

                return [];

                break;

            case 'drivers':
                $word = $request->word;

                $data = Driver::where('status', 'active')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name', 'mobile']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;

            case 'vendors':
                $word = $request->word;
                $data = User::where('status', 'active')
                    ->where('type','company')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;
            case 'users':
                $word = $request->word;

                $data =  User::where('status', 'active')
                    ->where('type','person')
                    ->where(function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id','name']);

                if(!$data) return [];

                $returnData = [];
                foreach ($data as $key => $value){
                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->name.'('.$value->id.')'];
                }
                return $returnData;

                break;


            case 'staff':
                $word = $request->word;


                $data = Staff::where('status', 'active')
                    ->where(function ($query) use ($word) {
                        $query->where('firstname', 'LIKE', '%' . $word . '%')
                            ->orWhere('lastname', 'LIKE', '%' . $word . '%')
                            ->orWhere('mobile', 'LIKE', '%' . $word . '%');
                    })
                    ->get(['id',
                        \DB::raw('CONCAT(firstname," ",lastname) as value')
                    ]);

                if(!$data) return [];
                return $data;

//                $returnData = [];
//                foreach ($data as $key => $value){
//                    $returnData[] =  ['id'=> $value->id, 'value'=> $value->firstname.' '.$value->lastname];
//                }
//
//                return $returnData;

                break;
            case 'area':
                $word = $request->word;

                $data = Area::where(function($query) use ($word) {
                    $query->where('name_ar','LIKE','%'.$word.'%')
                        ->orWhere('name_en','LIKE','%'.$word.'%');
                })->get([
                    'id'
                ]);

                if($data->isEmpty()){
                    return [];
                }

                $result = [];

                foreach ($data as $key => $value){
                    $result[] = [
                        'id'=> $value->id,
                        'value'=> str_replace($word,'<b>'.$word.'</b>',implode(' -> ',AreasData::getAreasUp($value->id,true) ))
                    ];

                    if(setting('area_select_type') == '2'){
                        $areaDown = AreasData::getAreasDown($value->id);
                        if(count($areaDown) > 1){
                            array_shift($areaDown);
                            foreach ($areaDown as $aK => $aV){
                                $result[] = [
                                    'id'=> $aV,
                                    'value'=> str_replace($word,'<b>'.$word.'</b>',implode(' -> ',AreasData::getAreasUp($aV,true) ))
                                ];
                            }
                        }
                    }

                }

                return $result;

                break;


        }

    }
    public function district($id){
        $district =  DB::table('districts')->where('city_id',$id)->select('id','name_'.clang().' as name')->get();
        return $this->success('Done', $district);
    }

    public function language(Request $request)
    {
        Session::put('lang', $request->lang??'ar');
        return response()->json(['Status' => 'success','message'=>$request->lang],200);
    }

    protected function success($msg='Done',$data = []){
        return $this->response(true,$msg,$data);
    }

    protected function fail($msg='fail',$data = []){
        return $this->response(false,$msg,$data);
    }

    protected function response($status,$message = 'Done',$data = [],$code = '200'): array {
        return [
            'status'=> $status,
            'code'=> $code,
            'message'=> $message,
            'data'=> $data
        ];
    }


}