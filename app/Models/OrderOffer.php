<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OrderOffer extends Model
{
    protected $table = 'order_offers';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable =
        [
            'id',
            'order_id',
            'order_status_id',
            'user_id',
            'comment',
        ];

    public function status(){
        return $this->belongsTo('App\Models\OrderStatus','order_status_id');
    }
    public function order(){
        return $this->belongsTo('App\Models\Order','order_id');
    }

    public function driver(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
