<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContactUs extends Model
{
    protected $table = 'contact_us';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable = [ 'id','name','email','phone','message'];
}
