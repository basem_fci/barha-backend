<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class District extends Model
{
    protected $table = 'districts';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable = ['id','city_id','name_ar','name_en','name_ur','code'];
    public function city(){
        return $this->belongsTo('App\Models\City','city_id');
    }
}
