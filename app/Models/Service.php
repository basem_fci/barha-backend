<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Service extends Model
{
    protected $table = 'services';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable = [ 'id','image','title_ar','title_en','title_ur','title_bn','description_ar','description_en','description_ur','description_bn','slug'];
}
