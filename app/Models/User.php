<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use  Notifiable;
    use SoftDeletes;
    protected $table = 'users';
    public $timestamps = true;

    use LogsActivity;
    protected static $logAttributes = ['*'];

    protected $dates = ['created_at','updated_at'/*,'deleted_at'*/];
    protected $fillable = [
        'type',
        'name',
        'status',
        'email',
        'mobile',
        'password',
        'download_manager_number',
        'logistic_manager_number',
        'password',
        'picture',
        'picture_car_registration_form',
        'picture_resident_id',
        'picture_working_card_transport',
        'picture_driver_license',
        'commercial_registration_number',
        'tax_number',
        'national_address',
    ];



    public function addresses(){
        return $this->hasMany('App\Models\Address','user_id');
    }


//    public function findForPassport($username){
//            return $this->where('username', $username)->first();
//    }


    public function orders(){
        return $this->hasMany('App\Models\Order','creatable_id')
            ->where('creatable_type','App\Models\User');
    }
    public function driver_orders(){
        return $this->hasMany('App\Models\Order','driver_id');
    }

    public function plan(){
        return $this->belongsTo('App\Models\Plan');
    }

    public function transactions()
    {
        return $this->morphMany('App\Models\Transaction', 'sign');
    }

//    public function AauthAcessToken(){
//        return $this->hasMany('\App\OauthAccessToken');
//    }


}
