<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OrderHistory extends Model
{
    protected $table = 'order_histories';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable =
        [
            'id',
            'order_id',
            'order_status_id',
            'creatable_id',
            'creatable_type',
            'comment',
        ];
    public function creatable()
    {
        return $this->morphTo();
    }

    public function creatorhistory()
    {
        return $this->morphOne('App\Models\Staff', 'creatable','creatable_type','id');
    }

    public function status(){
        return $this->belongsTo('App\Models\OrderStatus','order_status_id');
    }
    public function order(){
        return $this->belongsTo('App\Models\Order','order_id');
    }
}
