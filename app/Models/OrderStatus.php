<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;



class OrderStatus extends Model
{
    protected $table = 'order_statuses';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $fillable = [ 'id','name_ar','name_en','name_ur','name_bn','shortcut'];

}

