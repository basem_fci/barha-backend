<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model
{
    protected $table = 'orders';
    public $timestamps = true;
    use SoftDeletes;
    use LogsActivity;
    protected static $logAttributes = ['*'];
    protected $dates = ['deliver_date','created_at','updated_at'];

    protected $fillable =
        [
            'id',
            'order_status_id',
            'driver_id',
            'city_id_from',
            'city_id_to',
            'district_id_from',
            'district_id_to',
            'creatable_id',
            'creatable_type',
            'cargo_weight',
            'payload_type',
            'price',
            'total_price',
            'tracking_number',
            'address',
            'deliver_date',
        ];

    public function record($order_status_id,$comment = null,$created_type = null)
    {
        $record = new OrderHistory();
        $record->order_id  = $this->id;
        $record->order_status_id  = $order_status_id;
        $record->creatable_id = Auth::user()->id;
        $record->creatable_type =$created_type??'';
        $record->comment = $comment;
        if ($record->save()) {
            return true;
        }
        return false;
    }

    public function addOffer($order_status_id,$comment = null)
    {
        $record = new OrderOffer();
        $record->order_id  = $this->id;
        $record->order_status_id  = $order_status_id;
        $record->user_id = Auth::user()->id;
        $record->comment = $comment;
        if ($record->save()) {
            return true;
        }
        return false;
    }


    public function cityFrom(){
        return $this->belongsTo('App\Models\City','city_id_from');
    }

    public function cityTo(){
        return $this->belongsTo('App\Models\City','city_id_to');
    }

    public function district(){
        return $this->belongsTo('App\Models\District','district_id');
    }

    public function districtFrom(){
        return $this->belongsTo('App\Models\District','district_id_from');
    }

    public function districtTo(){
        return $this->belongsTo('App\Models\District','district_id_to');
    }
    public function status(){
        return $this->belongsTo('App\Models\OrderStatus','order_status_id');
    }
    public function driver(){
        return $this->belongsTo('App\Models\User','driver_id');
    }
    public function history(){
        return $this->hasMany('App\Models\OrderHistory','order_id');
    }
    public function offer(){
        return $this->hasMany('App\Models\OrderOffer','order_id');
    }
    public function creatable()
    {
        return $this->morphTo();
    }
}
