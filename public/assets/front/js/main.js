window.onscroll = function() { myFunction() };

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

$(window).on('load', function() {
    $('body').css("overflow", "auto");
    $('.loader').css("display", "none");
});

new WOW().init();

const main_swiper = new Swiper('.main-slider .swiper', {
    direction: 'horizontal',
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

main_swiper.on('slideChange', function() {
    new WOW().init();
});

const testi_swiper = new Swiper('.testimonial .swiper', {
    direction: 'horizontal',
    loop: true,
    autoplay: true,
    slidesPerView: "auto",
    centeredSlides: true,
    spaceBetween: 30,
    centeredSlides: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

const adv_swiper = new Swiper('.order .swiper', {
    direction: 'horizontal',
    loop: true,
    autoplay: true,
    slidesPerView: 1,
    centeredSlides: true,
    spaceBetween: 40,
    freeMode: true,
    breakpoints: {
        640: {
            slidesPerView: 2,
        },
        992: {
            slidesPerView: 2,
        },
        1024: {
            slidesPerView: 3,
        },
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

var uluru = { lat: 24.774265, lng: 46.738586 };

// Initialize and add the map
function initMap() {
    // The location of Uluru
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 10,
        center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
        draggable: false,
    });
}