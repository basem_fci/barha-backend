@extends('web.layout')
@section('header')
@endsection
@section('title')
    {{__('Home')}}
@endsection
@section('content')
    <section class="main-slider">
        <div class="swiper">
            <div class="swiper-wrapper">
                @foreach($slider as $slider)
                    <div class="swiper-slide">
                        <div class="overlay"></div>
                        <img src="{{url('storage/'.$slider->image)}}">
                    </div>
                @endforeach
            </div>
            <div class="swiper-button-prev">
            </div>
            <div class="swiper-button-next">
            </div>
        </div>
    </section>
    <!-- End of main slider section -->

    <section class="order featured ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <span class="section-title"> {{__('Featured Order')}}</span>
                </div>
                <div class="swiper">
                    <div class="swiper-wrapper">
                        @foreach($featured_order as $featured_orders)
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="content">
                                    <ul>
                                        <li>
                                            <span>
                                                <img src="{{url('assets/front/imgs/adv/business.png')}}"> {{__('Company name')}} : </span>
                                            <span>
                                                {{$featured_orders->creatable_type == "App\Models\Staff"?__('Barha'):$featured_orders->creatable->name??''}}
                                            </span>
                                        </li>
                                        <li>
                                            <span> <img src="{{url('assets/front/imgs/adv/package.png')}}"> {{__('Payload type')}} : </span>
                                            <span>{{$featured_orders->payload_type}} </span>
                                        </li>
                                        <li>
                                            <span> <img src="{{url('assets/front/imgs/adv/weight.png')}}"> {{__('Cargo Weight')}} : </span>
                                            <span>{{$featured_orders->cargo_weight}} </span>
                                        </li>
                                        <li>
                                            <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('From')}} : </span>
                                            <span> {{$featured_orders->cityFrom->{'name_'.clang()}  }}  -  {{ $featured_orders->districtFrom->{'name_'.clang()}  }} </span>
                                        </li>
                                        <li>
                                            <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('To')}} : </span>
                                            <span> {{$featured_orders->cityTo->{'name_'.clang()}  }}  -  {{ $featured_orders->districtTo->{'name_'.clang()}  }} </span>
                                        </li>
                                    </ul>

                                    <div class="more-details d-flex justify-content-between">
                                        <span> {{$featured_orders->deliver_date}}</span>
                                        <a href="{{route('web.order-details',$featured_orders->tracking_number)}}"> {{__('More details')}} <i class="fa fa-arrow-right"></i> </a>
                                    </div>
                                    <div class="more-link">
                                        <a href="{{route('web.order-details',$featured_orders->tracking_number)}}"  title="Add offer"><span class="fa fa-eye"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of featured Order section -->

    <section class="services ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <span class="section-title"> {{__('Our services')}}   </span>
                </div>
                @foreach($service as $service)
                <div class="col-lg-4 col-md-6">
                    <div class="service-item text-center">
                        <div class="service-icon">
                            <img src="{{url('storage/'.$service->image)}}" alt="{{$service->{'title_'.clang()}??'' }}">
                        </div>
                        <div class="content">
                            <h3> {{$service->{'title_'.clang()} }}</h3>
                            <p>
                                {{substr(strip_tags($service->{'description_'.clang()} ),0,50)}}
                            </p>
                            <a class="arrow-icon" href="{{route('web.service-details',$service->slug)}}">{{__('Read more')}} <i class="fa fa-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>
    <!-- End of services section -->

    <section class="order ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <span class="section-title"> {{__('Latest Order')}}</span>
                </div>
                @foreach($last_order as $last_orders)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="item">
                        <div class="content">
                            <ul>
                                <li>
                                    <span> <img src="{{url('assets/front/imgs/adv/business.png')}}"> {{__('Company name')}} : </span>
                                    <span>
                                                {{$last_orders->creatable_type == "App\Models\Staff"?__('Barha'):$last_orders->creatable->name??''}}
                                            </span>
                                </li>
                                <li>
                                    <span> <img src="{{url('assets/front/imgs/adv/package.png')}}"> {{__('Payload type')}} : </span>
                                    <span>{{$last_orders->payload_type}} </span>
                                </li>
                                <li>
                                    <span> <img src="{{url('assets/front/imgs/adv/weight.png')}}"> {{__('Cargo Weight')}} : </span>
                                    <span>{{$last_orders->cargo_weight}} </span>
                                </li>
                                <li>
                                    <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('From')}} : </span>
                                    <span> {{$last_orders->cityFrom->{'name_'.clang()}  }}  -  {{ $last_orders->districtFrom->{'name_'.clang()}  }} </span>
                                </li>
                                <li>
                                    <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('To')}} : </span>
                                    <span> {{$last_orders->cityTo->{'name_'.clang()}  }}  -  {{ $last_orders->districtTo->{'name_'.clang()}  }} </span>
                                </li>
                            </ul>

                            <div class="more-details d-flex justify-content-between">
                                <span> {{$last_orders->deliver_date}}</span>
                                <a href="{{route('web.order-details',$last_orders->tracking_number)}}">{{__('More details')}} <i class="fa fa-arrow-right"></i> </a>
                            </div>
                            <div class="more-link">
                                <a href="{{route('web.order-details',$last_orders->tracking_number)}}"  title="Add offer"><span class="fa fa-eye"></span></a>
                            </div>
                        </div>
                    </div>
                    </div>
                @endforeach
                <div class="col-12 text-center">
                    <a href="{{route('web.orders')}}" class="btn btn-primary my_btn"> {{__('More Orders')}} </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End of latest Order section -->

    <section class="about-us ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us">
                        <h3 class="section-title">{{__('About us')}} </h3>
                        <p class="testimonial-text">
                            {!! substr(strip_tags(setting('about_company_'.clang())),0,249) !!}

                        </p>
                        <div class="mt-3">
                            <a href="{{route('web.about-us')}}" class="btn btn-primary my_btn">{{__('Read more')}} </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <img src="{{url('assets/front/imgs/about-us.jpg')}}" alt="About-us image">
                </div>
            </div>
        </div>
    </section>

    {{--<section class="testimonial ptb-40">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-4">--}}
                    {{--<div>--}}
                        {{--<span class="section-title"> Customers feedbacks </span>--}}
                        {{--<h3> What They’re Talking About Company </h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-8">--}}
                    {{--<div class="swiper">--}}
                        {{--<div class="swiper-wrapper">--}}
                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/1.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Kevin Martin</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/2.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Jhon smith</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/3.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Kevin Martin</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="swiper-pagination"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- End of latest Order section -->

@endsection
@section('footer')

@endsection