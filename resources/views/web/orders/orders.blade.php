@extends('web.layout')
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Orders')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="order ptb-40">
        <div class="container">
            <div class="row">
                @foreach($orders as $order)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="item">
                            <div class="content">
                                <ul>
                                    <li>
                                        <span> <img src="{{url('assets/front/imgs/adv/business.png')}}"> {{__('Company name')}} : </span>
                                        <span>
                                                {{$order->creatable_type == "App\Models\Staff"?__('Barha'):$order->creatable->name??''}}
                                            </span>
                                    </li>
                                    <li>
                                        <span> <img src="{{url('assets/front/imgs/adv/package.png')}}"> {{__('Payload type')}} : </span>
                                        <span>{{$order->payload_type}} </span>
                                    </li>
                                    <li>
                                        <span> <img src="{{url('assets/front/imgs/adv/weight.png')}}"> {{__('Cargo Weight')}} : </span>
                                        <span>{{$order->cargo_weight}} </span>
                                    </li>
                                    <li>
                                        <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('From')}} : </span>
                                        <span> {{$order->cityFrom->{'name_'.clang()}  }}  -  {{ $order->districtFrom->{'name_'.clang()}  }} </span>
                                    </li>
                                    <li>
                                        <span> <img src="{{url('assets/front/imgs/adv/city.png')}}"> {{__('To')}} : </span>
                                        <span> {{$order->cityTo->{'name_'.clang()}  }}  -  {{ $order->districtTo->{'name_'.clang()}  }} </span>
                                    </li>
                                </ul>

                                <div class="more-details d-flex justify-content-between">
                                    <span> {{$order->deliver_date}}</span>
                                    <a href="{{route('web.order-details',$order->tracking_number)}}"> {{__('More details')}} <i class="fa fa-arrow-right"></i> </a>
                                </div>
                                <div class="more-link">
                                    <a href="{{route('web.order-details',$order->tracking_number)}}"  title="{{__('Show Order Details')}}"><span class="fa fa-eye"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-12 mt-2">
                    <nav class="d-flex justify-content-center">
                        <ul class="pagination">
                           {{$orders->render()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')

@endsection