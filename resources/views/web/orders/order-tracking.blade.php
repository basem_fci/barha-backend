@extends('web.layout')
@section('header')

@endsection
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Order Tracking')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tracking ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                        <i class="fas fa-truck font-22 avatar-title text-primary"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">   {{$order->driver->name??'' }} </span>
                                        </h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Driver')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                        <i class="fas fa-info font-22 avatar-title text-info"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                 {{$order->status->{'name_'.clang()} }} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Status')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                        <i class="fas fa-map-marker-alt font-22 avatar-title text-warning"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                            {{$order->tracking_number}}     </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Tracking Number')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>



                <div class="col-12">
                   <div class="mapouter">
                       <div id="map" style="width: 100%;height: 500px"></div>

                       {{--<div class="gmap_canvas">--}}
                            {{--<div id="map-canvas"></div>--}}

                            {{--<!--         <iframe class="w-100 h-100" id="gmap_canvas"--}}
                                       {{--src="https://maps.google.com/maps?q=cairo&t=&z=11&ie=UTF8&iwloc=&output=embed"--}}
                                       {{--frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>--}}
                                {{---->--}}
                        {{--</div>--}}
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')
    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0spXs-pROKxyVGNwmahwVTCOcIQgGVSk&callback=initMap&libraries=&v=weekly"
            defer
    ></script>
    <script type="text/javascript">
        var map;
        var markers = [];
        function initMap() {
            const haightAshbury = {lat: 24.774265, lng: 46.738586};
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 6,
                center: haightAshbury,
                mapTypeId: "terrain",
            });
        }
        $(document).ready(function () {
            $.get('{{route('web.misc.ajax')}}?type=available_driver&driver_id={{$order->driver_id}}', function (data) {
                load_locations(data)
            })
        })
        setInterval(function () {
            $.get('{{route('web.misc.ajax')}}?type=available_driver&driver_id={{$order->driver_id}}', function (data) {
                load_locations(data)
            })
        }, 10000)
        function load_locations(data) {
            deleteMarkers();
            for (var i = 0; i < data.length; i++) {
                var driver = data[i];
                console.log(driver);
                var driver_location = {lat: parseFloat(driver.lat), lng: parseFloat(driver.lng)};
                addMarker(driver_location, driver.id, driver.name,1)
            }
        }
        // Adds a marker to the map and push to the array.
        function addMarker(location, driver_id, driver_name,orders_count) {
            const marker = new google.maps.Marker({
                position: location,
                map: map,
                driver_id: driver_id,
                title: driver_name,
                icon : {    url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"   }
            });
            marker.icon = {url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"}
            marker.title = driver_name + '-'+orders_count;
            const infowindow = new google.maps.InfoWindow({
                content: '<div id="content">' + driver_name + '</div>',
            });
            marker.addListener("click", () => {
                infowindow.open(map, marker);
            });
            // infowindow.open(map, marker);
            markers.push(marker);
        }
        function clearMarkers() {
            setMapOnAll(null);
        }
        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }
        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (let i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }
    </script>

@endsection