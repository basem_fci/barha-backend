@extends('web.layout')
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Order details')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="order-details ptb-40">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h3 class="mb-0"> {{__('Order details')}} </h3>
                <div>
                    @if(Auth::user()->type =='driver' && $order->order_status_id ==3)
                        <a href="{{route('web.confirm-order',$order->tracking_number)}}" title="{{__('Confirm Order')}}" class="btn btn-success my_btn_success">
                            <i class="fas fa-check"></i> {{__('Confirm Order')}}
                        </a>
                    @endif
                    @if(Auth::user()->type =='driver' && $order->order_status_id ==4)
                        <a href="{{route('web.delivered-order',$order->tracking_number)}}" title="{{__('Delivered Order')}}" class="btn btn-success my_btn_success">
                            <i class="fas fa-check"></i> {{__('Delivered Order')}}
                        </a>
                            <a href="{{route('web.return-order',$order->tracking_number)}}" title="{{__('Return Order')}}" class="btn btn-success my_btn">
                                <i class="fas fa-times"></i> {{__('Return Order')}}
                        </a>
                    @endif
                    @if(Auth::user()->type =='company' && $order->order_status_id ==5)
                        <a href="{{route('web.confirm-delivered-order',$order->tracking_number)}}" title="{{__('Confirm Delivered Order')}}" class="btn btn-success my_btn_success">
                            <i class="fas fa-check"></i> {{__('Confirm Delivered Order')}}
                        </a>
                    @endif
                    @if(Auth::user()->type =='company' && $order->order_status_id ==6)
                        <a href="{{route('web.reship-order',$order->tracking_number)}}" title="{{__('Reship Order')}}" class="btn btn-success my_btn_success">
                            <i class="fas fa-check"></i> {{__('Reship Order')}}
                        </a>
                    @endif
                    <a href="{{route('web.tracking',$order->tracking_number)}}" title="Track order" class="btn btn-primary my_btn"> <i class="fa fa-map-marker" aria-hidden="true"></i> </a>
                    @if(Auth::user()->type =='company')
                        <a href="javascript:void(0)" title="Printing" class="btn btn-primary my_btn">
                            <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                    @endif

                </div>
            </div>

            <div class="row">


                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-danger border-danger border">
                                        <i class="fa fa-money font-22 avatar-title text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                {{$order->total_price}} {{__('SAR')}} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Price')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                        <i class="fas fa-info font-22 avatar-title text-info"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                {{$order->status->{'name_'.clang()} }} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Status')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                        <i class="fa fa-box-open font-22 avatar-title text-warning"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                {{$order->cargo_weight}} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Cargo type')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                        <i class="fas fa-weight-hanging font-22 avatar-title text-success"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1">
                                            <span data-plugin="counterup">
                                                {{$order->payload_type}}
                                            </span>
                                        </h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Payload Type')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-dark border-dark border">
                                        <i class="fas fa-city font-22 avatar-title text-dark"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                              {{$order->cityFrom->{'name_'.clang()} }}  -   {{$order->districtFrom->{'name_'.clang()} }}</span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('From')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                        <i class="fas fa-city font-22 avatar-title text-primary"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                  {{$order->cityTo->{'name_'.clang()} }} - {{$order->districtTo->{'name_'.clang()} }}</span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('To')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-md-12">
                    <div class="widget-rounded-circle card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-secondary border-secondary border">
                                        <i class="fas fa-map-marker-alt font-22 avatar-title text-secondary"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup"> 66
                                                {{$order->cityFrom->address }} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Address')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>
                @if(in_array($order->order_status_id,[1,7]) && Auth::user()->type =='driver')
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary my_btn" data-toggle="modal" data-target="#offerModal">
                       {{__('Add offer')}} </button>
                </div>
                    @endif
            </div>
        </div>
    </section>
    <!-- End of order section -->

    @if(Auth::user()->type =='company')
    <div class="history ptb-10">
        <div class="container">
            <div class="row mt-3">
                <div class="col-12">
                    <h3> {{__('History')}} </h3>
                </div>
                <div class="col-12">
                    <div class="table-responsive orders">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{__('Tracking Number')}} #</th>
                                <th scope="col">{{__('Status')}}</th>
                                <th scope="col">{{__('Creator')}}</th>
                                <th scope="col">{{__('Comment')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->history as $history)
                                <tr>
                                    <th scope="row">{{$order->tracking_number}}</th>
                                    <td>
                                        <span class="status {{$history->status->color??''}}">
                                           {{$history->status->{'name_'.clang()} }}
                                        </span>
                                    </td>
                                    <td>  @if($history->creatable_type == 'App\Models\Staff')
                                            {{__('Barha')}}
                                        @else
                                            {{$history->creatable->name}}
                                        @endif
                                    </td>
                                    <th scope="row">{{$history->comment}}</th>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="history ptb-30">
        <div class="container">
            <div class="row mt-3">
                <div class="col-12">
                    <h3> {{__('Offer')}} </h3>
                </div>
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{__('Driver')}} #</th>
                                <th scope="col">{{__('Status')}}</th>
                                <th scope="col">{{__('Comment')}}</th>
                                <th scope="col">{{__('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->offer as $offer)

                                <tr>
                                    <th scope="row">
                                        {{$offer->driver->name??''}}
                                    </th>
                                    <th>
                                        <span class="status {{$order->color??''}}">
                                           {{$offer->status->{'name_'.clang()} }}
                                        </span>
                                    </th>
                                    <th scope="row">{{$offer->comment}}</th>
                                    <th>
                                        @if($offer->status->id ==2)
                                        <a href="{{route('web.accept-offer',$offer->id)}}" title="{{__('Accept Order')}}" class="btn btn-lg btn-success">
                                            <i class="fas fa-check"></i>
                                        </a>
                                        <a href="{{route('web.cancel-offer',$offer->id)}}" title="{{__('Cancel Order')}}" class="btn btn-lg btn-danger">
                                            <i class="fas fa-times"></i>
                                        </a>
                                        @endif
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- Modal -->
    <div class="modal fade" id="offerModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="offerModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="offerModalLabel">{{__('Add offer')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('web.add-offer')}}" method="POST">
                        @csrf
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                        <div class="form-group">
                            <textarea class="form-control" required id="notes" name="notes" rows="5" placeholder="{{__('Notes')}}"></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('Close')}}</button>
                            <button type="submit" class="btn btn-primary my_btn">{{__('Send offer')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer')

@endsection