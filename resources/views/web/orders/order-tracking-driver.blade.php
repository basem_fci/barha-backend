@extends('web.layout')
@section('header')

@endsection
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Order Tracking')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tracking ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                        <i class="fas fa-truck font-22 avatar-title text-primary"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">   {{$order->driver->name??'' }} </span>
                                        </h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Driver')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                        <i class="fas fa-info font-22 avatar-title text-info"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                 {{$order->status->{'name_'.clang()} }} </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Status')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget-rounded-circle card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                        <i class="fas fa-map-marker-alt font-22 avatar-title text-warning"></i>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="text-end">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                            {{$order->tracking_number}}     </span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{__('Tracking Number')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->
                        </div>
                    </div>
                    <!-- end widget-rounded-circle-->
                </div>
            </div>
            <p id="get_location"></p>

        </div>
    </section>

@endsection
@section('footer')
    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0spXs-pROKxyVGNwmahwVTCOcIQgGVSk&callback=initMap&libraries=&v=weekly" defer></script>
    <script>
        $(document).ready(function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    //  $.notify(position.coords.latitude+','+position.coords.longitude, "success");
                    console.log(position.coords.latitude)
                    $.ajax({
                        url: '{{route('web.update-location')}}',
                        type: "GET",
                        dataType: "json",
                        data:{lat:position.coords.latitude,lng:position.coords.longitude},
                        success: function (data)
                        {
                            console.log(data)
                        },
                        error: function (err) {
                        }
                    });
                });
            } else {
                console.log( "Geolocation is not supported by this browser.")
            }
        })

        setInterval(function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    console.log(position.coords.latitude)
                    $.ajax({
                        url: '{{route('web.update-location')}}',
                        type: "GET",
                        dataType: "json",
                        data:{lat:position.coords.latitude,lng:position.coords.longitude},
                        success: function (data)
                        {
                        },
                        error: function (err) {
                        }
                    });

                });
            } else {
                console.log( "Geolocation is not supported by this browser.")
            }
        }, 10000)

        function showPosition(position) {
            var x = document.getElementById("get_location");
            x.innerHTML = "Latitude: " + position.coords.latitude +
                "<br>Longitude: " + position.coords.longitude;
        }

    </script>

@endsection