@extends('web.layout')
@section('header')
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('title')
Dashboard
@endsection
@section('content')
    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('My profile')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="profile ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="profile-sidebar nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <div class="d-flex justify-content-center mb-3 mt-5">
                            <div class="profile-image">
                                <span> <i class="fa fa-edit"></i> </span>
                                <input type="file" accept=".png, .jpg, .jpeg">
                                <img src="{{url('storage/'.Auth::user()->picture)}}">
                            </div>
                        </div>
                        <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true"> <i class="far fa-user"></i> {{__('Profile info')}}</a>
                        <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false"> <i class="fa fa-lock"></i> {{__('Change password')}}</a>
                        <a class="nav-link" id="v-pills-orders-tab" data-toggle="pill" href="#v-pills-orders" role="tab" aria-controls="v-pills-orders" aria-selected="false"> <i class="fas fa-shopping-bag"></i> {{__('Orders')}}
                        </a>
                        <a class="nav-link" id="v-pills-add-adv-tab" data-toggle="pill" href="#v-pills-add-adv" role="tab" aria-controls="v-pills-add-adv" aria-selected="false"> <i class="fas fa-plus"></i> {{__('Add Order')}}
                        </a>
                        <a class="nav-link" id="logout"href="{{route('logout')}}" > <i class="fas fa-sign-out-alt"></i> {{__('Logout')}} </a>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <div class="form-box">
                                <h3>{{__('profile')}} </h3>
                                <form action="{{route('web.update-profile',Auth::id())}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label for="name" class="form-label"> {{__('Company Name')}} <em> * </em> </label>
                                        <input type="text" class="form-control" value="{{Auth::user()->name}}" required id="name" name="name" placeholder="{{__('Enter Company Name')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="AdminPhone" class="form-label">{{__("Mobile Company")}}<em> * </em>
                                        </label>
                                        <input type="tel" class="form-control"  value="{{Auth::user()->mobile}}" required id="mobile" name="mobile">
                                    </div>

                                    <div class="form-group">
                                        <label for="LogisticsNumber" class="form-label"> {{__('Mobile Download Manager')}}<em> *
                                            </em> </label>
                                        <input type="tel" class="form-control" value="{{Auth::user()->download_manager_number}}" required id="download_manager_number" name="download_manager_number">
                                    </div>

                                    <div class="form-group">
                                        <label for="LogisticsNumber" class="form-label"> {{__('Mobile Logistic Manager')}}<em> *
                                            </em> </label>
                                        <input type="tel" class="form-control" value="{{Auth::user()->logistic_manager_number}}" required id="logistic_manager_number" name="logistic_manager_number">
                                    </div>

                                    <div class="form-group">
                                        <label for="companyNumber" class="form-label"> {{__('Company Profile Picture')}} <em> * </em>
                                        </label>
                                        <input type="file" class="form-control"  value="" id="picture" name="picture">
                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-primary my_btn">{{__('Save changes')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End of profile tab -->
                        <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                            <div class="form-box">
                                <h3>Change your password </h3>
                                <form action="{{route('web.change-password',Auth::id())}}" method="POST">
                                    {{csrf_field()}}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label for="oldPassword" class="form-label"> {{__('Old password')}} <em> * </em> </label>
                                        <input type="password" class="form-control" required id="currant_password" name="currant_password" placeholder="********">
                                    </div>

                                    <div class="form-group">
                                        <label for="new-password" class="form-label"> {{__('New password')}} <em> * </em>
                                        </label>
                                        <input type="password" class="form-control" required id="password" name="password" placeholder="********">
                                    </div>

                                    <div class="form-group">
                                        <label for="confirmPassword" class="form-label">{{__('Confirm new password')}} <em> *
                                            </em>
                                        </label>
                                        <input type="password" class="form-control" required id="password_confirmation" name="password_confirmation" placeholder="********">
                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-primary my_btn"> {{__('Change password')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End of change password tab -->

                        <div class="tab-pane fade orders" id="v-pills-orders" role="tabpanel" aria-labelledby="v-pills-orders-tab">
                            <div class="form-box">
                                <h3> {{__('Orders')}} </h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">{{__('Traking Number')}} #</th>
                                            <th scope="col">{{__('Status')}}</th>
                                            <th scope="col">{{__('Deliver Date')}}</th>
                                            <th scope="col">{{__('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(Auth::user()->orders as $orders)
                                            <tr>
                                                <th scope="row">{{$orders->tracking_number}}</th>
                                                <td>
                                                    <span class="status pending"> {{$orders->status->{'name_'.clang()} }} </span>
                                                </td>
                                                <td>{{$orders->deliver_date->format('Y-m-d')}}</td>
                                                <td>
                                                    <a href="{{route('web.order-details',$orders->tracking_number)}}">
                                                        <i class="fa fa-arrow-right"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End of orders tab -->

                        <div class="tab-pane fade add-adv" id="v-pills-add-adv" role="tabpanel" aria-labelledby="v-pills-add-adv-tab">
                            <div class="form-box">
                                <h3>{{__('Add Order')}} </h3>
                                @php
                                    $route = isset($result) ? route('web.orders.update',$result->id): route('web.orders.store');
                                @endphp
                                <form method="POST" action="{{$route}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($result))
                                        {{ method_field('PATCH') }}
                                    @endif
                                    <input type="hidden" name="creatable_id" value="{{auth()->user()->id}}" class="form-control" id="creatable_id-form-input" autocomplete="off">
                                    <input type="hidden" name="creatable_type" value="App\Models\User" class="form-control" id="creatable_type-form-input" autocomplete="off">
                                    <div class="k-portlet__body">
                                        <div id="form-alert-message"></div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>{{__('Cargo Weight')}}<span class="red-star">*</span></label>
                                                <input type="text" name="cargo_weight" value="{{$result->cargo_weight??old('cargo_weight')}}" required  class="form-control" id="cargo_weight-form-input" autocomplete="off">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('Payload Type')}}<span class="red-star">*</span></label>
                                                <input type="text" name="payload_type" value="{{$result->payload_type??old('payload_type')}}" required  class="form-control" id="payload_type-form-input" autocomplete="off">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('Price')}}<span class="red-star">*</span></label>
                                                <input type="text" name="price" value="{{$result->price??old('price')}}" required  class="form-control" id="payload_type-form-input" autocomplete="off">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('Tracking Number')}}<span class="red-star">*</span></label>
                                                <input type="text" name="tracking_number" value="{{$result->tracking_number??old('tracking_number')}}" required  class="form-control" id="tracking_number-form-input" autocomplete="off">
                                            </div>
                                            {{--                        {{dd($result->deliver_date)}}--}}
                                            <div class="col-md-6">
                                                <label>{{__('Deliver Date')}}<span class="red-star">*</span></label>
                                                <input type="date" name="deliver_date" value="{{$result->deliver_date??old('deliver_date')}}" required  class="form-control datepicker" id="deliver_date-form-input" autocomplete="off">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('Address')}}<span class="red-star">*</span></label>
                                                <input type="text" name="address" value="{{$result->address??old('address')}}" required  class="form-control" id="address-form-input" autocomplete="off">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('City From')}}<span class="red-star">*</span></label>
                                                <select name="city_id_from"  class="form-control" onchange="CityFromFunction(this.value,'district_id_from')" id="city_id_from" required autocomplete="off">
                                                    <option selected disabled>{{__('Select City From')}}</option>
                                                    @foreach($city as $city_from)
                                                        <option value="{{$city_from->id}}" @if(isset($result) && $result->city_id_from ==$city_from->id) selected @endif >{{$city_from->{'name_'.clang()} }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('District From')}}<span class="red-star">*</span></label>
                                                <select name="district_id_from"  class="form-control"  id="district_id_from" required autocomplete="off">
                                                    <option selected disabled>{{__('Select District From')}}</option>
                                                    @if(isset($district_from))
                                                        @foreach($district_from as $district_from)
                                                            <option value="{{$district_from->id}}" @if(isset($result) && $result->district_id_from ==$district_from->id) selected @endif >{{$district_from->{'name_'.clang()} }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('City To')}}<span class="red-star">*</span></label>
                                                <select name="city_id_to"  class="form-control" onchange="CityFromFunction(this.value,'district_id_to')" id="city_id_to" required autocomplete="off">
                                                    <option selected disabled>{{__('Select City To')}}</option>
                                                    @foreach($city as $city_from)
                                                        <option value="{{$city_from->id}}" @if(isset($result) && $result->city_id_to ==$city_from->id) selected @endif >{{$city_from->{'name_'.clang()} }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{__('District To')}}<span class="red-star">*</span></label>
                                                <select name="district_id_to"  class="form-control"  id="district_id_to" required autocomplete="off">
                                                    <option selected disabled>{{__('Select District To')}}</option>
                                                    @if(isset($district_to))
                                                        @foreach($district_to as $district_to)
                                                            <option value="{{$district_to->id}}" @if(isset($result) && $result->district_id_to  ==$district_to->id) selected @endif >{{$district_to->{'name_'.clang()} }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="k-portlet__foot">
                                            <div class="k-form__actions">
                                                <div class="row" style="float: right;">
                                                    <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!-- End of add adv tab -->

                        <div class="tab-pane fade order-details" id="v-pills-order-details" role="tabpanel" aria-labelledby="v-pills-order-details-tab">
                            <div class="form-box">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <h3 class="mb-0"> {{__('Order details ')}}</h3>
                                    <div>
                                        <a href="tracking.html" title="Track order" class="btn btn-primary my_btn"> <i class="fa fa-map-marker"></i> </a>
                                        <a href="javascript:void(0)" title="Printing" class="btn btn-primary my_btn">
                                            <i class="fa fa-print"></i> </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-danger border-danger border">
                                                            <i class="fa fa-money font-22 avatar-title text-danger"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                                    500 EGP </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">{{__('Price')}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                                            <i class="fas fa-info font-22 avatar-title text-info"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                                    Pending </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">Status</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                                            <i class="fa fa-box-open font-22 avatar-title text-warning"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                                    type </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">Cargo type</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                                            <i class="fas fa-weight-hanging font-22 avatar-title text-success"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup"> 5
                                                                    KG </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">Cargo wheight</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-dark border-dark border">
                                                            <i class="fas fa-city font-22 avatar-title text-dark"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                                    Nasr city </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">City</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                                            <i class="fas fa-city font-22 avatar-title text-primary"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">
                                                                    seventh Avenue </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">Quarter</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>

                                    <div class="col-md-12">
                                        <div class="widget-rounded-circle card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <div class="avatar-lg rounded-circle bg-soft-secondary border-secondary border">
                                                            <i class="fas fa-map-marker-alt font-22 avatar-title text-secondary"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-end">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup"> 66
                                                                    Road Broklyn Street, 600
                                                                    New York, USA </span></h3>
                                                            <p class="text-muted mb-1 text-truncate">Address</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row-->
                                            </div>
                                        </div>
                                        <!-- end widget-rounded-circle-->
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-12">
                                        <h3> Order history </h3>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-responsive orders">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order #</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Creator</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">1050017AS</th>
                                                    <td>
                                                        <span class="status delivered"> Delivered </span>
                                                    </td>
                                                    <td> Admin </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">1050017AS</th>
                                                    <td>
                                                        <span class="status pending"> Pending </span>
                                                    </td>
                                                    <td> Admin </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of order details tab -->
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('footer')
{{--    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>--}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0spXs-pROKxyVGNwmahwVTCOcIQgGVSk&callback=initMap&libraries=&v=weekly" defer></script>
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script>
        function CityFromFunction(val,district_id) {
            $.ajax({
                url: '{{url('district')}}' + '/' + val,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('#'+district_id).empty();
                    $('#'+district_id).append('<option disabled selected >Select District  </option>');
                    $.each(data.data, function (key, value) {
                        $('#'+district_id).append('<option value="' + value['id'] + '">' +
                            value['name'] + '</option>');
                    });
                },
                error: function (err) {
                },
                complete: function (data) {
                }
            });
        }
    </script>
@endsection