@extends('web.layout')
@section('content')
    <section class="register ptb-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="#company" data-toggle="tab">{{__('Register as company')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#driver" data-toggle="tab">{{__('Register as driver')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content form-box">
                        <div class="tab-pane active" id="company">
                            <h3>{{__('Register as company')}} </h3>
                            <form action="{{route('web.register-company-post')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="company" name="type" id="type">
                                <input type="hidden" value="active" name="status" id="status">
                                <div class="form-group">
                                    <label for="name" class="form-label"> {{__('Company Name')}} <em> * </em> </label>
                                    <input type="text" class="form-control"  value="{{old('name')}}" required id="name" name="name" placeholder="{{__('Enter Company Name')}}">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="form-label"> {{__('Company commercial registration number')}} <em> * </em> </label>
                                    <input type="text" class="form-control"  value="{{old('commercial_registration_number')}}" id="commercial_registration_number" name="commercial_registration_number" placeholder="{{__('Company commercial registration number')}}">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="form-label"> {{__('Tax number')}} <em> * </em> </label>
                                    <input type="text" class="form-control"  value="{{old('tax_number')}}" id="tax_number" name="tax_number" placeholder="{{__('Add tax number')}}">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="form-label"> {{__('National Address')}} <em> * </em> </label>
                                    <input type="text" class="form-control"  value="{{old('national_address')}}" id="national_address" name="tax_number" placeholder="{{__('Add tax number')}}">
                                </div>
                                <div class="form-group">
                                    <label>{{__('Mobile')}}<span class="red-star">*</span></label>
                                    <div class="input-group">
                                               <span class="input-group-text">
                                                    966
                                                </span>
                                        {!! Form::tel('mobile', null,['class'=>'form-control','required'=>'required','id'=>'mobile-form-input','autocomplete'=>'off' ,'pattern'=>'(05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})',
                                        'oninvalid'=>'this.setCustomValidity("'.__('Not valid mobile number').'")',
                                        'oninput'=>'this.setCustomValidity("")',
                                       ]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="download_manager_number" class="form-label"> {{__('Mobile Download Manager')}}<em> *
                                        </em> </label>
                                    <input type="tel" class="form-control"  value="{{old('download_manager_number')}}" required id="download_manager_number" name="download_manager_number">
                                </div>

                                <div class="form-group">
                                    <label for="LogisticsNumber" class="form-label"> {{__('Mobile Logistic Manager')}}<em> *
                                        </em> </label>
                                    <input type="tel" class="form-control"  value="{{old('logistic_manager_number')}}" required id="logistic_manager_number" name="logistic_manager_number">
                                </div>
                                <div class="form-group">
                                    <label for="password" class="form-label"> {{__('Password')}}<em> *
                                        </em> </label>
                                    <input type="password" class="form-control" value="" required id="password" name="password">
                                </div>
                                <div class="form-group">
                                    <label for="LogisticsNumber" class="form-label"> {{__('Confirm Password')}}<em> *
                                        </em> </label>
                                    <input type="password" class="form-control" value="" required id="password_confirmation" name="password_confirmation">
                                </div>

                                <div class="form-group">
                                    <label for="picture" class="form-label"> {{__('Company Profile Picture')}} <em> * </em>
                                    </label>
                                    <input type="file" class="form-control"  value="" id="picture" name="picture">
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-primary my_btn"> {{__('Save changes')}} </button>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="driver" >
                            <h3>{{__('Register as driver')}}</h3>
                            <form method="POST" action="{{route('web.register-driver-post')}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="active" name="status" id="status">
                                <input type="hidden" name="type" value="driver" class="form-control" id="type-form-input" autocomplete="off">
                                <div class="k-portlet__body">
                                    <div id="form-alert-message"></div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>{{__('Name')}}<span class="red-star">*</span></label>
                                            <input type="text" name="name" value="{{old('name')}}" required class="form-control" id="name-form-input" autocomplete="off">
                                        </div>

                                        <div class="col-md-12">
                                            <label>{{__('E-mail')}}<span class="red-star">*</span></label>
                                            <input type="text" name="email" value="{{old('email')}}" required class="form-control" id="email-form-input" autocomplete="off">
                                        </div>
                                        <div class="col-md-12">
                                            <label>{{__('Mobile')}}<span class="red-star">*</span></label>
                                            <div class="input-group">
                                               <span class="input-group-text">
                                                    966
                                                </span>
                                                {!! Form::tel('mobile', null,['class'=>'form-control','required'=>'required','id'=>'mobile-form-input','autocomplete'=>'off' ,'pattern'=>'(05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})',
                                                'oninvalid'=>'this.setCustomValidity("'.__('Not valid mobile number').'")',
                                                'oninput'=>'this.setCustomValidity("")',
                                               ]) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>{{__('Nationality')}}<span class="red-star">*</span></label>
                                            <select name="nationality"  class="form-control select2"  id="nationality-form-input" required autocomplete="off">
                                                <option selected disabled>{{__('Select Nationality')}}</option>
                                                <option value="saudi" @if(isset($result->status) && $result->status =='active') selected @endif >{{__('Saudi')}}</option>
                                                <option value="another" @if(isset($result->status) && $result->status =='in-active') selected @endif>{{__('Another')}}</option>
                                            </select>
                                        </div>
                                        {{--<div class="col-md-12">--}}
                                            {{--<label>{{__('Profile Picture')}}<span class="red-star">*</span></label>--}}
                                            {{--<input type="file" name="picture" value="" class="form-control" id="picture-form-input"  enctype="multipart/form-data" autocomplete="off">--}}
                                            {{--<div class="invalid-feedback" id="picture-form-error"></div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-12">
                                            <label>{{__('picture Car Registration Form')}}<span class="red-star">*</span></label>
                                            <input type="file" name="picture_car_registration_form" value="" class="form-control"  enctype="multipart/form-data" id="picture_car_registration_form-form-input" autocomplete="off">
                                            <div class="invalid-feedback" id="picture_car_registration_form-form-error"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>{{__('picture Resident Id')}}<span class="red-star">*</span></label>
                                            <input type="file" name="picture_resident_id" value="" class="form-control"  enctype="multipart/form-data" id="picture_resident_id-form-input" autocomplete="off">
                                            <div class="invalid-feedback" id="picture_resident_id-form-error"></div>
                                        </div>
                                        {{--<div class="col-md-12">--}}
                                            {{--<label>{{__('picture Working Card Transport')}}<span class="red-star">*</span></label>--}}
                                            {{--<input type="file" name="picture_working_card_transport" value="" class="form-control"  enctype="multipart/form-data" id="picture_working_card_transport-form-input" autocomplete="off">--}}
                                            {{--<div class="invalid-feedback" id="picture_working_card_transport-form-error"></div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-12">
                                            <label>{{__('picture Driver License')}}<span class="red-star">*</span></label>
                                            <input type="file" name="picture_driver_license" value="" class="form-control"  enctype="multipart/form-data" id="picture_driver_license-form-input" autocomplete="off">
                                            <div class="invalid-feedback" id="picture_driver_license-form-error"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>{{__('Password')}}<span class="red-star">*</span></label>
                                            <input type="password" name="password" value="" class="form-control" id="password-form-input" autocomplete="off">
                                            <div class="invalid-feedback" id="password-form-error"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>{{__('Confirm password')}}<span class="red-star">*</span></label>
                                            <input type="password" name="password_confirmation" value="" class="form-control" id="password_confirmation-form-input" autocomplete="off">
                                            <div class="invalid-feedback" id="password_confirmation-form-error"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary my_btn"> {{__('Save changes')}} </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="register-acc text-center mt-3">
                            <a href="{{route('web.login')}}"> {{__('Have an account ?')}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')

@endsection