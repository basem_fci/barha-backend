@extends('web.layout')
@section('content')
    <section class="login ptb-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <div class="form-box">
                        <h3 class="text-center">{{__('Login')}}</h3>
                        <form action="{{route('web.login-post')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="mobile" class="form-label"> {{__('Mobile')}} <em> * </em> </label>
                                <input type="tel" class="form-control" required id="mobile" name="mobile" placeholder="{{__('Enter Mobile')}}">
                            </div>

                            <div class="form-group">
                                <label for="password" class="form-label"> {{__('Password')}} <em> * </em> </label>
                                <input type="password" class="form-control" required id="password" name="password" placeholder="********">
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="rememberMe">
                                    <label class="custom-control-label" for="rememberMe">{{__('Remember me')}}</label>
                                </div>
                            </div>

                            <div class="d-flex align-items-center justify-content-between">
                                <button type="submit" class="btn btn-primary my_btn"> {{__('Login')}} </button>
                                <a href="{{route('forget.password.get')}}"> {{__('Forgot password ?')}} </a>
                            </div>
                        </form>

                        <div class="register-acc text-center mt-3">
                            <a href="{{route('web.register')}}"> {{__('Create new account')}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')

@endsection