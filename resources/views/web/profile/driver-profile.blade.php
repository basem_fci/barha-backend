@extends('web.layout')
@section('header')
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('title')
Dashboard
@endsection
@section('content')
    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('My profile')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="profile ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="profile-sidebar nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <div class="d-flex justify-content-center mb-3 mt-5">
                            <div class="profile-image">
                                <span> <i class="fa fa-edit"></i> </span>
                                <input type="file" accept=".png, .jpg, .jpeg">
                                <img src="{{url('storage/'.Auth::user()->picture)}}">
                            </div>
                        </div>
                        <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true"> <i class="far fa-user"></i> {{__('Profile info')}}</a>
                        <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false"> <i class="fa fa-lock"></i>{{__('Change password')}}</a>
                        <a class="nav-link" id="v-pills-orders-tab" data-toggle="pill" href="#v-pills-orders" role="tab" aria-controls="v-pills-orders" aria-selected="false"> <i class="fas fa-shopping-bag"></i> {{__('My Orders')}}
                        <a class="nav-link"  href="{{route('web.orders')}}" > <i class="fas fa-shopping-bag"></i> {{__('All Orders')}}</a>
                        <a class="nav-link" id="logout"href="{{route('logout')}}" > <i class="fas fa-sign-out-alt"></i> {{__('Logout')}} </a></a>
                    </div>

                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <div class="form-box">
                                <h3>{{__('Profile info')}} </h3>
                                <form action="{{route('web.update-profile',Auth::id())}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label for="name" class="form-label"> {{__('Name')}} <em> * </em> </label>
                                        <input type="text" class="form-control" value="{{Auth::user()->name}}" required id="name" name="name" placeholder="Enter your name">
                                    </div>

                                    <div class="form-group">
                                        <label for="AdminPhone" class="form-label"> {{__('Mobile')}} <em> * </em>
                                        </label>
                                        <input type="tel" class="form-control"  value="{{Auth::user()->mobile}}" required id="mobile" name="mobile">
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="form-label"> {{__('E-mail')}}
                                        </label>
                                        <input type="email" class="form-control"  value="{{Auth::user()->email}}" required id="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="Nationality" class="form-label">{{__('Nationality')}} <em> * </em>
                                        </label>
                                        <select name="nationality" id="nationality" class="form-control">
                                            <option disabled selected>{{__('Select Nationality')}}</option>
                                            <option value="saudi" @if(Auth::user()->nationality == 'saudi') selected @endif>{{__('Saudi')}}</option>
                                            <option value="other" @if(Auth::user()->nationality == 'other') selected @endif>{{__('Other')}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="form-label"> {{__('Picture')}}
                                        </label>
                                        <input type="file" class="form-control"  value=""  id="picture" name="picture">
                                    </div>
                                    <div class="form-group">
                                        <label for="picture" class="form-label"> {{__('picture Working Card Transport')}}
                                        </label>
                                        <input type="file" class="form-control"  value=""  id="picture" name="picture">
                                    </div>
                                    <div class="form-group">
                                        <label for="picture" class="form-label"> {{__('picture Car Registration Form')}}
                                        </label>
                                        <input type="file" class="form-control"  value=""  id="picture" name="picture">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary my_btn"> {{__('Save changes')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End of profile tab -->

                        <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                            <div class="form-box">
                                <h3>{{__('Change password')}} </h3>
                                <form action="{{route('web.change-password',Auth::id())}}" method="POST">
                                    {{csrf_field()}}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label for="oldPassword" class="form-label"> {{__('Old password')}} <em> * </em> </label>
                                        <input type="password" class="form-control" required id="currant_password" name="currant_password" placeholder="********">
                                    </div>

                                    <div class="form-group">
                                        <label for="new-password" class="form-label"> {{__('New password')}} <em> * </em>
                                        </label>
                                        <input type="password" class="form-control" required id="password" name="password" placeholder="********">
                                    </div>

                                    <div class="form-group">
                                        <label for="confirmPassword" class="form-label">{{__('Confirm new password')}} <em> *
                                            </em>
                                        </label>
                                        <input type="password" class="form-control" required id="password_confirmation" name="password_confirmation" placeholder="********">
                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-primary my_btn"> {{__('Change password')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End of change password tab -->

                        <div class="tab-pane fade orders" id="v-pills-orders" role="tabpanel" aria-labelledby="v-pills-orders-tab">
                            <div class="form-box">
                                <h3> {{__('Orders')}} </h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">{{__('Traking Number')}} #</th>
                                            <th scope="col">{{__('Status')}}</th>
                                            <th scope="col">{{__('Deliver Date')}}</th>
                                            <th scope="col">{{__('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(Auth::user()->driver_orders as $orders)
                                            <tr>
                                                <th scope="row">{{$orders->tracking_number}}</th>
                                                <td>
                                                    <span class="status pending"> {{$orders->status->{'name_'.clang()} }} </span>
                                                </td>
                                                <td>{{$orders->deliver_date->format('Y-m-d')}}</td>
                                                <td>
                                                    <a href="{{route('web.order-details',$orders->tracking_number)}}">
                                                        <i class="fa fa-arrow-right"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End of orders tab -->

                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('footer')



@endsection