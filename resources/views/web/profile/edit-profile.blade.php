@extends('web.layout')
@section('header')
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('title')
    Your Personal Info
@endsection
@section('content')
    <div class="px-10 pt-10 bg-brand-blue-dark">
        <div class="row">
            <div class="col-lg-12">
                <!-- Salute + Small stats -->
                <div class="row align-items-center mb-4">
                    <div class="col-md-5 mb-4 mb-md-0">
                        <h1 class="mb-1 text-white">Personal information</h1>
                        <span class="text-white-80"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-surface-secondary pt-18 pb-18 border-bottom-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="mb-3"> Your Personal Info </h3>
                            <form action="{{route('web.update-profile',Auth::user()->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                {{ method_field('PATCH') }}
                                <div class="mb-5">
                                    <label class="form-label" for="name">Avatar
                                        @if(isset(Auth::user()->image) && !empty(Auth::user()->image))
                                            <span class="avatar bg-soft-warning text-warning rounded-circle">
                                                <img src="{{url('storage/'.Auth::user()->image)}}">
                                            </span>
                                        @endif
                                    </label>
                                    <input type="file" class="form-control form-control-muted" id="image" name="image" enctype="multipart/form-data">
                                </div>
                                <div class="mb-5">
                                    <label class="form-label" for="name">Full name</label>
                                    <input type="text" value="{{Auth::user()->name}}" class="form-control form-control-muted" id="name" name="name">
                                </div>
                                <div class="mb-5">
                                    <label class="form-label" for="email">Email address</label>
                                    <input type="email" value="{{Auth::user()->email}}" class="form-control form-control-muted" id="email" name="email">
                                </div>
                                <div class="mb-5">
                                    <label class="form-label" for="phone">Phone</label>
                                    <input type="tel" value="{{Auth::user()->mobile}}" class="form-control form-control-muted" id="mobile" name="mobile">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary w-full">
                                        Save changes
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="mb-3"> Change password </h3>
                            <form action="{{route('web.change-password')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                {{ method_field('PATCH') }}                                <div class="mb-5">
                                    <label class="form-label" for="password">Password</label>
                                    <input type="password" class="form-control form-control-muted" name="currant_password" id="currant_password" autocomplete="password">
                                </div>
                                <div class="mb-5">
                                    <label class="form-label" for="newPassword">New password</label>
                                    <input type="password" class="form-control form-control-muted" name="password" id="password" autocomplete="confirm-password">
                                </div>
                                <div class="mb-5">
                                    <label class="form-label" for="confirmPassword">Confirm password</label>
                                    <input type="password" class="form-control form-control-muted" name="password_confirmation" id="password_confirmation" autocomplete="confirm-password">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary w-full">
                                        Change password
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('footer')

@endsection