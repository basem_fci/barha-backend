<!doctype html>
<html lang="en">
@php
    $logo = url('storage/app/'.setting('image'));
@endphp
<head>
    <!-- meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <link  rel="shortcut icon" type="img/png" href="{{$logo}}" rel="stylesheet" type="text/css" />
    <script src="https://kit.fontawesome.com/6b81245e26.js" crossorigin="anonymous"></script>
    <link href="https://unpkg.com/@webpixels/css@1.0/dist/index.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather:300,400" rel="stylesheet">
    <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/custom.index.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />

    <title>ABAR-Login</title>
</head>

<body>

<!-- PRE LOADER -->
<div class="preloader">
    <div class="sk-spinner sk-spinner-wordpress">
        <span class="sk-inner-circle"></span>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light px-0 py-3">
    <div class="container-xl">
        <!-- Logo -->
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{url('assets/front/images/logo-h.png')}}" class="">
        </a>
        <!-- Navbar toggle -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarCollapse">

        @if(auth()->user())
            <!-- Nav -->
                <ul class="navbar-nav ms-auto no-mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('web.myprojects')}}">My Projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('web.dashboard')}}">Dashboard</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{route('web.support')}}">Support</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/about')}}">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/faq')}}">FAQ</a>
                    </li>
                </ul>
                <div class="d-flex align-items-lg-center mt-3 mt-lg-0">
                    <div class="position-relative d-inline-block text-white">
                    <span class="avatar bg-soft-warning text-warning rounded-circle">
                       @if(isset(Auth::user()->image) && !empty(Auth::user()->image))
                            <img src="{{url('storage/'.Auth::user()->image)}}">
                        @else
                            {{substr(Auth::user()->name??'',0,2)}}
                        @endif
                    </span>
                        <span class="position-absolute bottom-2 end-2 transform translate-x-1/2 translate-y-1/2 border-2 border-solid border-current w-3 h-3 bg-success rounded-circle"></span>
                    </div>
                    <div class="ms-2">
                        <span class="d-block text-sm font-semibold">
						Hi , {{Auth::user()->name??''}}
					</span>
                        <span class="d-block text-xs text-muted font-regular">
						{{Auth::user()->email??''}}
					</span>
                    </div>
                </div>
        @else
            <!-- Nav -->
                <ul class="navbar-nav ms-auto no-mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">Homepage</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/about')}}">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/faq')}}">FAQ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/members')}}">Members</a>
                    </li>
                </ul>
                <div class="d-flex align-items-lg-center mt-3 mt-lg-0">
                    <a href="{{url('login')}}" class="btn btn-sm btn-gold btn-primary w-full w-lg-auto">
                        <i class="fa fa-user pe-2"></i>  Login
                    </a>
                </div>
            @endif
        </div>
    </div>
</nav>
    <div class="px-5 py-5 p-lg-0 bg-surface-secondary">
        <div class="d-flex justify-content-center">
            <div class="col-lg-4 col-xl-4 p-12 p-xl-20 position-fixed start-0 top-0 h-screen overflow-y-hidden bg-brand-blue d-none d-lg-flex flex-column" style="z-index: 1;">
                <!-- Title -->
                <div class="mt-16 mb-20">
                    <h1 class="ls-tight font-bolder display-6 text-white mb-5">
                        Let’s build something amazing today.
                    </h1>
                    <p class="text-white-80">
                        Maybe some text here will help me see it better. Welcome to Abar!
                    </p>
                </div>
                <!-- Circle -->
                <div class="w-56 h-56 bg-brand-gold rounded-circle position-absolute bottom-0 end-20 transform translate-y-1/3">
                </div>
            </div>
            <div class="col-12 col-md-9 col-lg-8 offset-lg-5 border-left-lg min-h-lg-screen d-flex flex-column justify-content-center py-lg-16 px-lg-20 position-relative">
                <div class="row">

                    @include('web.error')
                    <div class="col-lg-10 col-md-9 col-xl-6 mx-auto ms-xl-0">
                        <div class="mt-10 mt-lg-5 mb-6 d-flex align-items-center d-lg-block">
                            <span class="d-inline-block d-lg-block h1 mb-lg-6 me-3">👋</span>
                            <h1 class="ls-tight font-bolder h2">
                                Reset Your Password!
                            </h1>
                        </div>
                        @include('web.error')
                        <form action="{{route('forget.password.get')}}" method="post">
                            @csrf
                            <div class="mb-5">
                                <label class="form-label" for="email">Email address</label>
                                <input type="email" value="{{old('email')}}" placeholder="ex:info@abar.com"  class="form-control form-control-muted" id="email" name="email" required>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary w-full">
                                    Rest Password
                                </button>
                            </div>
                        </form>
                        <div class="pt-3 text-center">
                            <span class="text-xs text-uppercase font-semibold">or</span>
                        </div>

                        <div class="my-1 text-center">
                            <small>have an account?</small>
                            <a href="{{route('web.login')}}" class="text-warning text-sm font-semibold">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- footer -->
<footer class="pt-14 bg-surface-secondary border-top">
    <div class="container max-w-screen-xl">
        <div class="row align-items-center justify-content-md-between pb-7">
            <div class="col-md-6">
                <div class="copyright text-sm text-center text-md-start">
                    Copyright &copy; 2021 <a href="#" class="h6 text-sm font-bold">Abar</a>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0 mx-n4">
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">
                            Privacy Policy
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">
                            Terms &amp; Conditions
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<a href="#back-top" class="go-top"><i class="fa fa-angle-up"></i></a>
<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<script src="{{asset('assets/front/js/jquery.js')}}"></script>
<script src="{{asset('assets/front/js/particles.min.js')}}"></script>
<script src="{{asset('assets/front/js/jquery.parallax.js')}}"></script>
<script src="{{asset('assets/front/js/smoothscroll.js')}}"></script>
<script src="{{asset('assets/front/js/custom.js')}}"></script>

</body>
</html>
