@extends('web.layout')
@section('header')
@endsection
@section('title')
    {{__('Service')}}
@endsection
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Services')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services bg-1 ptb-40">
        <div class="container">
            <div class="row">
                @foreach($service as $services)
                    <div class="col-lg-4 col-md-6">
                        <div class="service-item text-center">
                            <div class="service-icon">
                                <img src="{{url('storage/'.$services->image)}}" alt="{{$services->{'title_'.clang()}??'' }}">
                            </div>
                            <div class="content">
                                <h3> {{$services->{'title_'.clang()} }}</h3>
                                <p>
                                    {{substr(strip_tags($services->{'description_'.clang()} ),0,50)}}
                                </p>
                                <a class="arrow-icon" href="{{route('web.service-details',$services->slug)}}">{{__('Read more')}} <i class="fa fa-arrow-right"></i> </a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-12 mt-2">
                    <nav class="d-flex justify-content-center">
                        <ul class="pagination">
                            {{$service->render()}}
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End of services section -->
@endsection
@section('footer')

@endsection