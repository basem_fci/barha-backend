@extends('web.layout')
@section('header')
@endsection
@section('title')
    {{__('About Company')}}
@endsection
@section('content')
    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front//imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('About us')}}</h4>
                        <h5 class="h1 page-title">{{__('We Are Trusted')}}</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-us ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us">
                        <h2>{{__('About Barha')}} </h2>

                        <p class="testimonial-text">
                            {!! setting('about_company_'.clang()) !!}
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                    <img src="{{url('assets/front/imgs/about_us.jpg')}}" alt="About-us image">
                </div>
            </div>
        </div>
    </section>

    {{--<section class="testimonial ptb-40">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-4">--}}
                    {{--<div>--}}
                        {{--<span class="section-title"> Customers feedbacks </span>--}}
                        {{--<h3> What They’re Talking About Company </h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-8">--}}
                    {{--<div class="swiper">--}}
                        {{--<div class="swiper-wrapper">--}}
                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/1.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Kevin Martin</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/2.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Jhon smith</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="swiper-slide">--}}
                                {{--<div class="testimonial-item">--}}
                                    {{--<div class="client-info">--}}
                                        {{--<div class="client-img">--}}
                                            {{--<img src="./imgs/users/3.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="client-details">--}}
                                            {{--<h5 class="client-name">Kevin Martin</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="testimonial-quote">--}}
                                        {{--<i class="fas fa-quote-right"></i>--}}
                                    {{--</div>--}}
                                    {{--<p class="testimonial-text">Lorem ipsum is simply free text dolor sit amet, consectetur notted adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore text.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="swiper-pagination"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
@endsection
@section('footer')

@endsection