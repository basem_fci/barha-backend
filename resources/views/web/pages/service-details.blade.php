@extends('web.layout')
@section('header')
@endsection
@section('title')
    {{__('Service')}}
@endsection
@section('content')

    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front//imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Service')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-us ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us">
                        <h2>{{$service->{'title_'.clang()} }} </h2>

                        <p class="testimonial-text">
                            {!! $service->{'description_'.clang()}   !!}
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                    <img src="{{url('storage/'.$service->image)}}" alt="About-us image"style="
    width: 300px;
">
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')

@endsection