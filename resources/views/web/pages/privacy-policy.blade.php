@extends('web.layout')
@section('header')
@endsection
@section('title')
    Blog Details
@endsection
@section('content')
    <div class="px-10 bg-brand-blue-dark pt-7 pb-7">
        <div class="d-flex align-items-center justify-content-between">
            <h1 class="mb-1 text-white">Privacy policy</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Home Page</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Privacy policy</li>
                </ol>
            </nav>
        </div>
    </div>
    <section class="terms pt-10 pb-10">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    {!! setting('privacy_policy') !!}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')

@endsection