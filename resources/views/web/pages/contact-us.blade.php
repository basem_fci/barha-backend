@extends('web.layout')
@section('header')
@endsection
@section('title')
    {{__('Contact Us')}}
@endsection
@section('content')
    <section class="page-title-area sky-blue-bg pt-280 pb-180 pt-lg-200 pt-md-160 pb-md-120 pt-xs-160 pb-xs-90">
        <img class="page-shape shape_04 d-none d-md-inline-block" src="{{url('assets/front/imgs/breadcrumb/orange-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_06 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/berry-1.svg')}}" alt="Page Shape">
        <img class="page-shape shape_07 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/truck.svg')}}" alt="Page Shape">
        <img class="page-shape shape_08 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/dot-a.svg')}}" alt="Page Shape">
        <img class="page-shape shape_09 d-none d-lg-inline-block" src="{{url('assets/front/imgs/breadcrumb/nav-box.svg')}}" alt="Page Shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="page-title-wrapper text-center">
                        <h4 class="styled-text theme-color mb-30">{{__('Contact us')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-container">
                        <form action="{{route('web.contact-us-post')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label"> Name <em> * </em> </label>
                                <input type="text" class="form-control" required id="name" name="name" placeholder="Enter your name">
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="phone" class="form-label"> phone </label>
                                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter phone number">
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="email" class="form-label"> Email </label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="message" class="form-label"> Your message <em> * </em> </label>
                                <textarea class="form-control" id="message" name="message" required placeholder="Type here" rows="5"></textarea>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-primary my_btn"> Send message </button>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="contact-container">
                        <h3>{{__('Contact us')}} </h3>
                        <ul class="contact-address">
                            <li> <span> <i class="fas fa-map-marker-alt"></i> </span> {{setting('address_'.clang())}}</li>
                            <li> <span> <i class="fas fa-phone-volume"></i> </span>
                                <a href="tel: {{setting('mobile')}}"> {{setting('mobile')}}</a> </li>
                            <li> <span> <i class="far fa-envelope"></i></span> <a href="mailto:{{setting('email')}}">
                                    {{setting('email')}}</a> </li>
                        </ul>

                        <ul class="social-links mt-2 mb-2">
                            <li>
                                <a href="{{setting('facebook')}}" target="_blank"> <i class="fab fa-facebook-square"></i> </a>
                            </li>
                            <li>
                                <a href="{{setting('twitter')}}" target="_blank"> <i class="fab fa-twitter-square"></i> </a>
                            </li>
                            <li>
                                <a href="{{setting('instagram')}}" target="_blank"> <i class="fab fa-instagram"></i> </a>
                            </li>
                            <li>
                                <a href="{{setting('snapchat')}}" target="_blank"> <i class="fab fa-snapchat"></i> </a>
                            </li>
                        </ul>
                        <div class="text-right">
                            <img src="{{url('assets/front/imgs/contact.svg')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')

@endsection