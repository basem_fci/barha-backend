<!doctype html>
<html @if(lang() == 'en') lang="en" direction="ltr" @else lang="ar" direction="rtl" style="direction: rtl;" @endif>

@php
    $logo = url('storage/'.setting('logo'));

@endphp
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/d85efbc469.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
    <link href="{{asset('assets/front/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />
    @if(app()->getLocale() == 'ar' || app()->getLocale() == 'ur')
        <link href="{{asset('assets/front/css/rtl.css')}}" rel="stylesheet" type="text/css" />
    @endif

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    @if(app()->getLocale() == 'ar' || app()->getLocale() == 'ur')
        <link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
        <style>
            body {
                font-family: 'Cairo';
                font-size: 17px;
            }
        </style>
    @endif
    <link rel="shortcut icon" href="{{asset('logo/favicon.ico')}}" />
    @yield('header')
    <title>{{__('Barha')}} | @yield('title')</title>
</head>

<body>
<div class="loader">
    <img src="{{url('assets/front/imgs/Spinner.gif')}}">
</div>
<header>
    <div class="top-nav">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-sm-center text-lg-left">
                    <div class="dropdown d-inline-flex">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            @if(\App::getLocale() == 'ar')
                                <img src="{{url('assets/front/imgs/flags/ar.png')}}"> &nbsp;	 {{__('Arabic')}}
                                @elseif(\App::getLocale() == 'ur')
                                <img src="{{url('assets/front/imgs/flags/ur.png')}}"> &nbsp;	 {{__('Urdu')}}
                            @else
                                <img src="{{url('assets/front/imgs/flags/us.png')}}"> &nbsp;  {{__('English')}}
                            @endif
                            <b  class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li onclick="ChangeLanguage('ar')">
                                <a href=""> <img src="{{url('assets/front/imgs/flags/ar.png')}}">&nbsp; {{__('Arabic')}}</a>
                            </li>
                            <li onclick="ChangeLanguage('en')">
                                <a href=""> <img src="{{url('assets/front/imgs/flags/us.png')}}">&nbsp; {{__('English')}}</a>
                            </li>
                            <li onclick="ChangeLanguage('ur')">
                                <a href=""> <img src="{{url('assets/front/imgs/flags/ur.png')}}">&nbsp; {{__('Urdu')}}</a>
                            </li>
                        </ul>
                    </div>
                    <ul class="d-inline-flex ml-3">
                        <li>
                            <a href="tel:{{setting('mobile')}}"> <i class="fas fa-phone"></i> {{setting('mobile')}} </a>
                        </li>
                        <li>
                            <a href="mailto:{{setting('email')}}"> <i class="far fa-envelope"></i>{{setting('email')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 text-sm-center text-lg-right">
                    @if(Auth::user())
                        <a href="{{route('web.dashboard')}}"> <i class="fas fa-user-circle"></i> {{__('My profile')}}</a>
                    @else
                        <div class="dropdown d-inline-flex">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-user-circle"></i> {{__('Account')}} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{route('web.login')}}"> <i class="fas fa-sign-out-alt"></i> {{__('Login')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('web.register')}}"> <i class="fas fa-unlock-alt"></i>{{__('Register')}} </a>
                                </li>
                            </ul>
                        </div>
                    @endif
                    <ul class="d-inline-flex ml-3 social-links">
                        @if(!empty(setting('facebook')))
                            <li>
                                <a href="{{setting('facebook')}}" target="_blank"> <i class="fab fa-facebook-square"></i> </a>
                            </li>
                        @endif
                        @if(!empty(setting('twitter')))
                            <li>
                                <a href="{{setting('twitter')}}" target="_blank"> <i class="fab fa-twitter-square"></i> </a>
                            </li>
                        @endif
                        @if(!empty(setting('instagram')))
                            <li>
                                <a href="{{setting('instagram')}}" target="_blank"> <i class="fab fa-instagram"></i> </a>
                            </li>
                        @endif
                        @if(!empty(setting('snapchat')))
                            <li>
                                <a href="{{setting('snapchat')}}" target="_blank"> <i class="fab fa-snapchat"></i> </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <nav id="navbar" class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}"> <img src="{{$logo}}" alt="Logo image"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item  {{request()->url()==url('/') ? 'active' : ''}}">
                        <a class="nav-link" href="{{url('/')}}">{{__('Home')}}</a>
                    </li>
                    <li class="nav-item {{request()->url()==route('web.services') ? 'active' : ''}}" >
                        <a class="nav-link" href="{{route('web.services')}}" >{{__('Services')}}</a>
                    </li>
                    <li class="nav-item {{request()->url()==route('web.about-us') ? 'active' : ''}}">
                        <a class="nav-link" href="{{route('web.about-us')}}">{{__('About us')}}</a>
                    </li>
                    <li class="nav-item {{request()->url()==route('web.contact-us') ? 'active' : ''}}">
                        <a class="nav-link" href="{{route('web.contact-us')}}">{{__('Contact us')}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('content')
<footer class="ptb-40">
    <div class="container">
        <div class="site-footer-map"></div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="logo">
                    <img src="{{$logo}}" alt="Logo image">
                </div>

                <p class="about-text" style="color: #ffffff !important">
                    {!! substr(strip_tags(setting('about_company_'.clang())),0,249) !!}
{{--                    {{setting('about_company_'.clang())}}--}}
                </p>
                <ul class="social-links">
                    @if(!empty(setting('facebook')))
                        <li>
                            <a href="{{setting('facebook')}}" target="_blank"> <i class="fab fa-facebook-square"></i> </a>
                        </li>
                    @endif
                    @if(!empty(setting('twitter')))
                        <li>
                            <a href="{{setting('twitter')}}" target="_blank"> <i class="fab fa-twitter-square"></i> </a>
                        </li>
                    @endif
                    @if(!empty(setting('instagram')))
                        <li>
                            <a href="{{setting('instagram')}}" target="_blank"> <i class="fab fa-instagram"></i> </a>
                        </li>
                    @endif
                    @if(!empty(setting('snapchat')))
                        <li>
                            <a href="{{setting('snapchat')}}" target="_blank"> <i class="fab fa-snapchat"></i> </a>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="footer-widget-contact">
                    <h3 class="footer-widget-title">{{__('Pages')}}</h3>

                    <ul class="links-list">
                        <li><a href="{{route('web.services')}}">{{__('Services')}}</a></li>
                        <li><a href="{{route('web.about-us')}}">{{__('About')}}</a></li>
                        <li><a href="{{route('web.contact-us')}}">{{__('Contact us')}}</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="footer-widget-contact">
                    <h3 class="footer-widget-title">{{__('Contact')}}</h3>
                    <p class="about-text" style="color: #ffffff !important">{{setting('address_'.clang())}}
                    </p>
                    <ul class="contact-list">
                        <li>
                            <a href="tel:{{setting('mobile')}}"> <i class="fas fa-phone"></i> {{setting('mobile')}} </a>
                        </li>
                        <li>
                            <a href="mailto:{{setting('email')}}"> <i class="far fa-envelope"></i> {{setting('email')}} </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="copyright text-center">
    <div class="container">
        <div class="d-flex justify-content-between">
            <span> {{__('All rights reserved to Barha Corporation')}}</span>
            <a href="{{route('web.terms-conditions')}}"> {{__('Terms & conditions')}} </a>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script src="{{asset('assets/front/js/wow.min.js')}}"></script>
<script src="{{asset('assets/front/js/main.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@if ($errors->any())
    @php
        $all_error = '';
        foreach ($errors->all() as $error){
            $all_error =$all_error.' '.$error;
        }
    @endphp
    <script>
        Swal.fire({
            icon: "error",
            title: "{{ $all_error }}",
            showConfirmButton: false,
            // timer: 3000
        });
    </script>
@endif
<script>
    @php
        $success = session('success');
        $add= session('add');
        $created= session('created');
        $updated = session('updated');
        $deleted = session('deleted');
        $delete = session('delete');
        $err = session('error');

    @endphp

    @if (isset($success))
    Swal.fire({
        icon: "success",
        title: "{{ session('success') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($created))
    Swal.fire({
        icon: "success",
        title: "{{ session('created') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($add))
    Swal.fire({
        icon: "success",
        title: "{{ session('add') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($updated))
    Swal.fire({
        icon: "info",
        title: "{{session('updated') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($deleted))
    Swal.fire({
        icon: "danger",
        title: "{{ session('deleted') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($delete))
    Swal.fire({
        icon: "success",
        title: "{{ session('delete') }}",
        showConfirmButton: false,
        timer: 3000
    });
    @endif
    @if (isset($err))
    Swal.fire({
        icon: "error",
        title: "{{ session('error') }}",
        showConfirmButton: false,
        timer: 3000
    });

    @endif

    function ChangeLanguage(val)
    {

        $.ajax({
            url: '{{route('web.language.change')}}',
            type: "GET",
            dataType: "json",
            data:{lang:val},
            success: function (data)
            {
                console.log(data);

            },
            error: function (err) {
                console.log(err);
            },
            complete: function (data) {
               location.reload();
            }
        });
    }

</script>

@yield('footer')

</body>

</html>
