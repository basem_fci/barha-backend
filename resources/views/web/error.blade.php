
@if ($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger"> {{$error}}</div>
    @endforeach
@endif
@php
    $success = session('success');
    $add= session('add');
    $created= session('created');
    $updated = session('updated');
    $deleted = session('deleted');
    $delete = session('delete');
    $err = session('error');

@endphp

@if (isset($success))
    <div class="alert alert-success">{{$success}}</div>

@endif
@if (isset($created))
    <div class="alert alert-success">{{$created}}</div>

@endif
@if (isset($add))
    <div class="alert alert-success">{{$add}}</div>
@endif
@if (isset($updated))
    <div class="alert alert-warning">{{$updated}}</div>

@endif
@if (isset($deleted))
    <div class="alert alert-danger">{{$deleted}}</div>
@endif
@if (isset($delete))
    <div class="alert alert-danger">{{$delete}}</div>

@endif
@if (isset($err))
    <div class="alert alert-danger">{{$err}}</div>

@endif
