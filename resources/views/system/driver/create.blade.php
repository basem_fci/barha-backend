@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ? route('system.driver.update',$result->id): route('system.driver.store');
            @endphp
            <form method="POST" action="{{$route}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <input type="hidden" name="type" value="driver" class="form-control" id="type-form-input" autocomplete="off">
                <div class="k-portlet__body">
                    <div id="form-alert-message"></div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name" value="{{$result->name??''}}" required class="form-control" id="name-form-input" autocomplete="off">
                        </div>

                        <div class="col-md-6">
                            <label>{{__('E-mail')}}<span class="red-star">*</span></label>
                            <input type="text" name="email" value="{{$result->email??''}}" required class="form-control" id="email-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Mobile')}}<span class="red-star">*</span></label>
                            <input type="text" name="mobile" value="{{$result->mobile??''}}" required class="form-control" id="mobile-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Nationality')}}<span class="red-star">*</span></label>
                            <select name="nationality"  class="form-control select2"  id="nationality-form-input" required autocomplete="off">
                                <option selected disabled>{{__('Select Nationality')}}</option>
                                <option value="saudi" @if(isset($result->status) && $result->status =='active') selected @endif >{{__('Saudi')}}</option>
                                <option value="another" @if(isset($result->status) && $result->status =='in-active') selected @endif>{{__('Another')}}</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Profile Picture')}}<span class="red-star">*</span></label>
                            <input type="file" name="picture" value="" class="form-control" id="picture-form-input"  enctype="multipart/form-data" autocomplete="off">
                            <div class="invalid-feedback" id="picture-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('picture Car Registration Form')}}<span class="red-star">*</span></label>
                            <input type="file" name="picture_car_registration_form" value="" class="form-control"  enctype="multipart/form-data" id="picture_car_registration_form-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="picture_car_registration_form-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('picture Resident Id')}}<span class="red-star">*</span></label>
                            <input type="file" name="picture_resident_id" value="" class="form-control"  enctype="multipart/form-data" id="picture_resident_id-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="picture_resident_id-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('picture Working Card Transport')}}<span class="red-star">*</span></label>
                            <input type="file" name="picture_working_card_transport" value="" class="form-control"  enctype="multipart/form-data" id="picture_working_card_transport-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="picture_working_card_transport-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('picture Driver License')}}<span class="red-star">*</span></label>
                            <input type="file" name="picture_driver_license" value="" class="form-control"  enctype="multipart/form-data" id="picture_driver_license-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="picture_driver_license-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Status')}}<span class="red-star">*</span></label>
                            <select name="status" class="form-control select2"title="" id="status-form-input" required autocomplete="off">
                                <option selected disabled>{{__('Select Status')}}</option>
                                <option value="active" @if(isset($result->status) && $result->status =='active') selected @endif >{{__('Active')}}</option>
                                <option value="in-active" @if(isset($result->status) && $result->status =='in-active') selected @endif>{{__('In-Active')}}</option>
                            </select>
                            <div class="invalid-feedback" id="status-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Password')}}<span class="red-star">*</span></label>
                            <input type="password" name="password" value="" class="form-control" id="password-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="password-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Confirm password')}}<span class="red-star">*</span></label>
                            <input type="password" name="password_confirmation" value="" class="form-control" id="password_confirmation-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="password_confirmation-form-error"></div>
                        </div>
                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
@endsection
