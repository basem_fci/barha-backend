@extends('system.layout')
@section('header')
    <link href="{{asset('assets/custom/user/profile-v1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row clearfix">
        <!-- Basic Examples -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h2>
                        {{ $result->name }}
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Name')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->name) ? '-' : $result->name }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Status')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->status) ? '-' : $result->status }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Mobile')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->mobile) ? '-' : $result->mobile }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Picture')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    @if(isset($result->picture) && !empty($result->picture))
                                        <a href="{{url('storage/'.$result->picture)}}"
                                           onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                            <img src="{{url('storage/'.$result->picture)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                        </a>
                                    @endif
                                </div>
                            </div>
                                <div class="d-flex  border border-primary rounded mb-2">
                                    <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                        {{__('picture Car Registration Form')}}
                                    </div>
                                        <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                            @if(isset($result->picture_car_registration_form) && !empty($result->picture_car_registration_form))
                                                <a href="{{url('storage/'.$result->picture_car_registration_form)}}"
                                                   onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                                    <img src="{{url('storage/'.$result->picture_car_registration_form)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                                </a>
                                            @endif
                                        </div>
                                </div>
                                <div class="d-flex  border border-primary rounded mb-2">
                                    <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                        {{__('picture Resident Id')}}
                                    </div>
                                    <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                        @if(isset($result->picture_resident_id) && !empty($result->picture_resident_id))
                                            <a href="{{url('storage/'.$result->picture_resident_id)}}"
                                               onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                                <img src="{{url('storage/'.$result->picture_resident_id)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                            </a>
                                        @endif
                                    </div>
                                </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Nationality')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->nationality) ? '-' : $result->nationality }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('E-mail')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->email) ? '-' : $result->email }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Type')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->type) ? '-' : $result->type }}
                                </div>
                            </div>
                                <div class="d-flex  border border-primary rounded mb-2">
                                    <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                        {{__('picture Working Card Transport')}}
                                    </div>
                                    <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                        @if(isset($result->picture_working_card_transport) && !empty($result->picture_working_card_transport))
                                            <a href="{{url('storage/'.$result->picture_working_card_transport)}}"
                                               onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                                <img src="{{url('storage/'.$result->picture_working_card_transport)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-flex  border border-primary rounded mb-2">
                                    <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                        {{__('picture Driver License')}}
                                    </div>
                                        <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                            @if(isset($result->picture_driver_license) && !empty($result->picture_driver_license))
                                                <a href="{{url('storage/'.$result->picture_driver_license)}}"
                                                   onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                                    <img src="{{url('storage/'.$result->picture_driver_license)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                                </a>
                                            @endif
                                        </div>

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



