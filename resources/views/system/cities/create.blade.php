@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ? route('system.cities.update',$result->id): route('system.cities.store');
            @endphp
            <form method="POST" action="{{$route}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <div class="k-portlet__body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Arabic Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_ar" value="{{$result->name_ar??''}}" required class="form-control" id="name_ar-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('English Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_en" value="{{$result->name_en??''}}" required class="form-control" id="name_en-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Urdu Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_ur" value="{{$result->name_ur??''}}" required class="form-control" id="name_ur-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('City Code')}}<span class="red-star">*</span></label>
                            <input type="text" name="code" value="{{$result->code??''}}" required class="form-control" id="code-form-input" autocomplete="off">
                        </div>

                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
@endsection
