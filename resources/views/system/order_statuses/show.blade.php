@extends('system.layout')
@section('header')
    <link href="{{asset('assets/custom/user/profile-v1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row clearfix">
        <!-- Basic Examples -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h2>
                        {{ $result->name }}
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Arabic Name')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->name_ar) ? '-' : $result->name_ar }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('English Name')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->name_en) ? '-' : $result->name_en }}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Urdu Name')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->name_ur) ? '-' : $result->name_ur }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



