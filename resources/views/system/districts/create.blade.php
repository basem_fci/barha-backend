@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ? route('system.districts.update',$result->id): route('system.districts.store');
            @endphp
            <form method="POST" action="{{$route}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <div class="k-portlet__body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('City')}}<span class="red-star">*</span></label>
                            <select name="city_id" class="form-control select2">
                                <option disabled selected>{{__('Select City')}}</option>
                                @foreach($city as $cities)
                                    <option value="{{$cities->id}}"
                                    @if(isset($result) && $result->city_id == $cities->id) selected @endif>{{$cities->{'name_'.clang()} }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District Arabic Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_ar" value="{{$result->name_ar??''}}" required class="form-control" id="name_ar-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District English Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_en" value="{{$result->name_en??''}}" required class="form-control" id="name_en-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District Urdu Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name_ur" value="{{$result->name_ur??''}}" required class="form-control" id="name_ur-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District Code')}}<span class="red-star">*</span></label>
                            <input type="text" name="code" value="{{$result->code??''}}" required class="form-control" id="code-form-input" autocomplete="off">
                        </div>

                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
@endsection
