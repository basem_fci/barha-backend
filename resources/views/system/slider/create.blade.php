@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ? route('system.slider.update',$result->id):route('system.slider.store');
            @endphp
            <form action="{{$route}}" method="post" role="form" id="main-form" accept-charset="utf-8" enctype="multipart/form-data"  >
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <div class="k-portlet__body">

                    <div id="form-alert-message"></div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Image')}}<span class="red-star">*</span></label>
                            <input type="file" name="avatar" id="files" placeholder="{{__('Image')}}"  class="form-control"   enctype="multipart/form-data" @if(!isset($adv)) required @endif>
                            <div class="invalid-feedback" id="avatar-form-error"></div>
                        </div>

                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script type="text/javascript">

        function submitMainForm(){
            formSubmit(
                '{{isset($result) ? route('system.slider.update',$result->id):route('system.slider.store')}}',
                $('#main-form').serialize(),
                function ($data) {
                    window.location = $data.data.url;
                },
                function ($data){
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    pageAlert('#form-alert-message','error',$data.message);
                }
            );
        }

    </script>
@endsection
