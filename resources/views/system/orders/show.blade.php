@extends('system.layout')
@section('header')
    <link href="{{asset('assets/custom/user/profile-v1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row clearfix">
        <!-- Basic Examples -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h2>
                      {{__('Traking Number')}}  : <span>{{ $result->tracking_number }}</span>
                    </h2>
                </div>
{{--                {{dd($result )}}--}}
                <div class="card-body">
                    <div class="row clearfix">
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Status')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->status->{'name_'.clang()}??'' }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Driver')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->driver->name??'' }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('City From')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->cityFrom->{'name_'.clang()} }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('City To')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->cityTo->{'name_'.clang()} }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Cargo Weight')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->cargo_weight }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Payload Type')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->payload_type }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Deliver Date')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->deliver_date }}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Created By')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{$result->creatable->name??''}}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Created Type')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    @if($result->creatable_type == 'App\Models\Staff')
                                        Admin
                                    @else
                                        {{$result->creatable->type}}
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('District From')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->districtFrom->{'name_'.clang()} }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('District To')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->districtTo->{'name_'.clang()} }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Price')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->price }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Total Price')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->total_price }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Address')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ $result->address }}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12 col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class='card-title'>{{__('Order History')}}</h5>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover js-basic-example shipments-dataTable text-center">
                                            <thead>
                                            <tr>
                                                <th class='text-center'>{{__('ID')}}</th>
                                                <th class='text-center'>{{__('Order Status')}}</th>
                                                <th class='text-center'>{{__('Created By')}}</th>
                                                <th class='text-center'>{{__('Created Type')}}</th>
                                                <th class='text-center'>{{__('Comment')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $counter = 1;
                                            @endphp
                                            @foreach($result->history??[] as $key => $history)
                                            <tr>
                                                <td>{{$counter}}</td>
                                                <td>{{ $history->status->{'name_'.clang()}??'' }}</td>
                                                <td>{{$result->creatable->name??''}}</td>
                                                <td>
                                                    @if($history->creatable_type == 'App\Models\Staff')
                                                        Admin
                                                    @else
                                                        {{$history->creatable->type}}
                                                    @endif
                                                </td>
                                                <td>{{ $history->comment??'' }}</td>
                                            </tr>
                                            @php
                                                $counter++;
                                            @endphp
                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection



