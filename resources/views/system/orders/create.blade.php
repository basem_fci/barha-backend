@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ? route('system.orders.update',$result->id): route('system.orders.store');
            @endphp
            <form method="POST" action="{{$route}}" id="main-form" class="k-form"  enctype="multipart/form-data">
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <input type="hidden" name="creatable_id" value="{{auth()->user()->id}}" class="form-control" id="creatable_id-form-input" autocomplete="off">
                <input type="hidden" name="creatable_type" value="App\Models\Staff" class="form-control" id="creatable_type-form-input" autocomplete="off">
                <div class="k-portlet__body">
                    <div id="form-alert-message"></div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Cargo Weight')}}<span class="red-star">*</span></label>
                            <input type="text" name="cargo_weight" value="{{$result->cargo_weight??old('cargo_weight')}}" required  class="form-control" id="cargo_weight-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Payload Type')}}<span class="red-star">*</span></label>
                            <input type="text" name="payload_type" value="{{$result->payload_type??old('payload_type')}}" required  class="form-control" id="payload_type-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Price')}}<span class="red-star">*</span></label>
                            <input type="text" name="price" value="{{$result->price??old('price')}}" required  class="form-control" id="payload_type-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Tracking Number')}}<span class="red-star">*</span></label>
                            <input type="text" name="tracking_number" value="{{$result->tracking_number??old('tracking_number')}}" required  class="form-control" id="tracking_number-form-input" autocomplete="off">
                        </div>
{{--                        {{dd($result->deliver_date)}}--}}
                        <div class="col-md-6">
                            <label>{{__('Deliver Date')}}<span class="red-star">*</span></label>
                            <input type="text" name="deliver_date" value="{{$result->deliver_date??old('deliver_date')}}" required  class="form-control datepicker" id="deliver_date-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Address')}}<span class="red-star">*</span></label>
                            <input type="text" name="address" value="{{$result->address??old('address')}}" required  class="form-control" id="address-form-input" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label>{{__('City From')}}<span class="red-star">*</span></label>
                            <select name="city_id_from"  class="form-control" onchange="CityFromFunction(this.value,'district_id_from')" id="city_id_from" required autocomplete="off">
                                <option selected disabled>{{__('Select City From')}}</option>
                                @foreach($city as $city_from)
                                    <option value="{{$city_from->id}}" @if(isset($result) && $result->city_id_from ==$city_from->id) selected @endif >{{$city_from->{'name_'.clang()} }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District From')}}<span class="red-star">*</span></label>
                            <select name="district_id_from"  class="form-control"  id="district_id_from" required autocomplete="off">
                                <option selected disabled>{{__('Select District From')}}</option>
                                @if(isset($district_from))
                                    @foreach($district_from as $district_from)
                                        <option value="{{$district_from->id}}" @if(isset($result) && $result->district_id_from ==$district_from->id) selected @endif >{{$district_from->{'name_'.clang()} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('City To')}}<span class="red-star">*</span></label>
                            <select name="city_id_to"  class="form-control" onchange="CityFromFunction(this.value,'district_id_to')" id="city_id_to" required autocomplete="off">
                                <option selected disabled>{{__('Select City To')}}</option>
                                @foreach($city as $city_from)
                                    <option value="{{$city_from->id}}" @if(isset($result) && $result->city_id_to ==$city_from->id) selected @endif >{{$city_from->{'name_'.clang()} }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('District To')}}<span class="red-star">*</span></label>
                            <select name="district_id_to"  class="form-control"  id="district_id_to" required autocomplete="off">
                                <option selected disabled>{{__('Select District To')}}</option>
                                @if(isset($district_to))
                                    @foreach($district_to as $district_to)
                                        <option value="{{$district_to->id}}" @if(isset($result) && $result->district_id_to  ==$district_to->id) selected @endif >{{$district_to->{'name_'.clang()} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script>
        function CityFromFunction(val,district_id) {
            $.ajax({
                url: '{{url('system/district')}}' + '/' + val,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('#'+district_id).empty();
                    $('#'+district_id).append('<option disabled selected >Select Disssss  </option>');
                    $.each(data.data, function (key, value) {
                        $('#'+district_id).append('<option value="' + value['id'] + '">' +
                            value['name'] + '</option>');
                    });
                },
                error: function (err) {
                },
                complete: function (data) {
                }
            });
        }
    </script>
@endsection
