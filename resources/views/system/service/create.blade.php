@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">
            @php
                $route = isset($result) ?route('system.service.update',$result->id) :route('system.service.store');
            @endphp
            <form action="{{$route}}" method="post" role="form" id="main-form"  accept-charset="utf-8" enctype="multipart/form-data"  >
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <div class="k-portlet__body">

                    <div id="form-alert-message"></div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Title Arabic')}}<span class="red-star">*</span></label>
                            <input type="text" name="title_ar" value="{{$result->title_ar??''}}" id="title_ar" placeholder="{{__('Title Arabic')}}"  class="form-control" required >
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Title English')}}<span class="red-star">*</span></label>
                            <input type="text" name="title_en" value="{{$result->title_en??''}}" id="title_en" placeholder="{{__('Title English')}}"  class="form-control" required >
                        </div>

                        <div class="col-md-6">
                            <label>{{__('Description Arabic')}}<span class="red-star">*</span></label>
                            <textarea  name="description_ar" rows="10" id="description_ar"  class="form-control" >{{$result->description_ar??''}}</textarea>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Description English')}}<span class="red-star">*</span></label>
                            <textarea  name="description_en" rows="10" id="description_en"  class="form-control" >{{$result->description_en??''}}</textarea>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Image')}}<span class="red-star">*</span></label>
                            <input type="file" name="image" id="image" placeholder="{{__('Image')}}"  class="form-control" enctype="multipart/form-data">
                        </div>
                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $('#description_ar').summernote({
            tabsize: 5,
            height: 200,
        });
        $('#description_en').summernote({
            tabsize: 5,
            height: 200,
        });

    </script>
@endsection
