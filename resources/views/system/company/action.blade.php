<form method="post" id="delete-form-{{$data->id}}" action="{{ route('system.company.destroy',$data->id) }}">
    {{ csrf_field () }}
    {{method_field('Delete')}}
</form>
<span class="dropdown">
    <a href="#" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
      <i class="la la-gear"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-left " x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-36px, 25px, 0px);">
        <a class="dropdown-item" href="{{route('system.company.show',$data->id)}}" target="_blank"><i class="far fa-eye"></i>{{__('View')}}</a>
        <a class="dropdown-item" href="{{route('system.company.edit',$data->id)}}"><i class="la la-edit"></i> {{__('Edit')}}</a>
        <button form="delete-form-{{$data->id}}" class="dropdown-item" onclick="return confirm('{{__("Do you want to delete this record?")}}');"><i class="fa fa-trash"></i> {{__('Delete')}}</button>
    </div>
</span>