@extends('system.layout')
@section('content')
    <!--begin::Row-->
    <div class="card card-custom gutter-b">

        <div class="card-body">

            @php
                $route = isset($result) ? route('system.company.update',$result->id): route('system.company.store');
            @endphp
            <form method="POST" action="{{$route}}" id="main-form" class="k-form" enctype="multipart/form-data">
                @csrf
                @if(isset($result))
                    {{ method_field('PATCH') }}
                @endif
                <input type="hidden" name="type" value="company" class="form-control" id="type-form-input" autocomplete="off">
                <div class="k-portlet__body">
                    <div id="form-alert-message"></div>
                    <div class="form-group row">
                       <div class="col-md-6">
                            <label>{{__('Company Name')}}<span class="red-star">*</span></label>
                            <input type="text" name="name" value="{{$result->name??''}}" required class="form-control" id="name-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="name-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Mobile Company')}}<span class="red-star">*</span></label>
                            <input type="text" name="mobile" value="{{$result->mobile??''}}" required class="form-control" id="mobile-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="mobile-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Mobile Download Manager')}}<span class="red-star">*</span></label>
                            <input type="text" name="download_manager_number" value="{{$result->download_manager_number??''}}" class="form-control" id="download_manager_number-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="download_manager_number-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Mobile Logistic Manager')}}<span class="red-star">*</span></label>
                            <input type="text" name="logistic_manager_number" required value="{{$result->logistic_manager_number??''}}" class="form-control" id="logistic_manager_number-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="logistic_manager_number-form-error"></div>
                        </div>

                        <div class="col-md-6">
                            <label>{{__('Company Profile Picture')}}</label>
                            <input type="file" name="picture" value=""  class="form-control" id="picture-form-input"  enctype="multipart/form-data" autocomplete="off">
                            <div class="invalid-feedback" id="picture-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Status')}}<span class="red-star">*</span></label>
                            <select name="status" class="form-control" id="status-form-input" autocomplete="off">
                                <option selected disabled>{{__('Select Status')}}</option>
                                <option value="active" @if(isset($result->status) && $result->status =='active') selected @endif >{{__('Active')}}</option>
                                <option value="in-active" @if(isset($result->status) && $result->status =='in-active') selected @endif>{{__('In-Active')}}</option>
                            </select>
                            <div class="invalid-feedback" id="status-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Password')}}<span class="red-star">*</span></label>
                            <input type="password" name="password" value="" class="form-control" id="password-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="password-form-error"></div>
                        </div>
                        <div class="col-md-6">
                            <label>{{__('Confirm password')}}<span class="red-star">*</span></label>
                            <input type="password" name="password_confirmation" value="" class="form-control" id="password_confirmation-form-input" autocomplete="off">
                            <div class="invalid-feedback" id="password_confirmation-form-error"></div>
                        </div>
                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions">
                            <div class="row" style="float: right;">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('footer')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
@endsection
