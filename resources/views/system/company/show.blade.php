@extends('system.layout')
@section('header')
    <link href="{{asset('assets/custom/user/profile-v1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row clearfix">
        <!-- Basic Examples -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h2>
                        {{ $result->name }}
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Company Name')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->name) ? '-' : $result->name }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Status')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->status) ? '-' : $result->status }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Mobile Company')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->mobile) ? '-' : $result->mobile }}
                                </div>
                            </div>


                        </div>
                        <div class="col-6">
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Mobile Download Manager')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->download_manager_number) ? '-' : $result->download_manager_number }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Mobile Logistic Manager')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    {{ empty($result->logistic_manager_number) ? '-' : $result->logistic_manager_number }}
                                </div>
                            </div>
                            <div class="d-flex  border border-primary rounded mb-2">
                                <div class="w-100 d-flex align-items-center justify-content-center bg-primary p-3 text-light">
                                    {{__('Company Profile Picture')}}
                                </div>
                                <div class="w-100 p-4 text-center font-weight-bolder font-size-h6 ">
                                    @if(isset($result->picture) && !empty($result->picture))
                                        <a href="{{url('storage/'.$result->picture)}}"
                                           onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;">
                                            <img src="{{url('storage/'.$result->picture)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



