-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2020 at 09:26 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `empty_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `log_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(10) DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(10) DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- --------------------------------------------------------

--
-- Table structure for table `auth_api`
--

DROP TABLE IF EXISTS `auth_api`;
CREATE TABLE `auth_api` (
  `id` bigint(20) NOT NULL,
  `guard_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auth_api`
--

INSERT INTO `auth_api` (`id`, `guard_name`, `access_token`, `user_id`, `ip`, `user_agent`, `created_at`, `updated_at`) VALUES
(11, 'driver_api', '495e6f0214efffa42ba9389a68d38de75b6373d395e717567761dfe86da58b2a', 1, '::1', 'insomnia/2020.4.1', '2020-10-30 18:24:34', '2020-10-30 20:44:51'),
(14, 'api', 'fd58386affa34e5835c4aca75847707f0817b8532eae91c1b6039251bbc369a8', 4, '::1', 'insomnia/2020.4.1', '2020-10-30 20:25:05', '2020-10-30 20:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `auth_session`
--

DROP TABLE IF EXISTS `auth_session`;
CREATE TABLE `auth_session` (
  `id` bigint(20) NOT NULL,
  `guard_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auth_session`
--

INSERT INTO `auth_session` (`id`, `guard_name`, `access_token`, `user_id`, `ip`, `user_agent`, `created_at`, `updated_at`) VALUES
(37, 'staff', '6b41f4e6e8131fa33abee5aba95a5c06', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '2020-11-05 06:03:19', '2020-11-05 06:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_06_172833_create_activity_log_table', 1),
(9, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(10, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(11, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(12, '2016_06_01_000004_create_oauth_clients_table', 2),
(13, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `notifiable_id` int(10) NOT NULL,
  `notifiable_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) NOT NULL,
  `route_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_group_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `route_name`, `permission_group_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7585, 'system.payment-methods.index', 14, NULL, NULL, NULL),
(7586, 'system.payment-methods.show', 14, NULL, NULL, NULL),
(7587, 'system.payment-methods.destroy', 14, NULL, NULL, NULL),
(7588, 'system.payment-methods.show', 14, NULL, NULL, NULL),
(7589, 'system.payment-methods.create', 14, NULL, NULL, NULL),
(7590, 'system.payment-methods.store', 14, NULL, NULL, NULL),
(7591, 'system.payment-methods.edit', 14, NULL, NULL, NULL),
(7592, 'system.payment-methods.update', 14, NULL, NULL, NULL),
(7593, 'system.staff.index', 14, NULL, NULL, NULL),
(7594, 'system.staff.show', 14, NULL, NULL, NULL),
(7595, 'system.staff.create', 14, NULL, NULL, NULL),
(7596, 'system.staff.store', 14, NULL, NULL, NULL),
(7597, 'system.staff.edit', 14, NULL, NULL, NULL),
(7598, 'system.staff.update', 14, NULL, NULL, NULL),
(7599, 'system.permission-group.index', 14, NULL, NULL, NULL),
(7600, 'system.permission-group.show', 14, NULL, NULL, NULL),
(7601, 'system.permission-group.create', 14, NULL, NULL, NULL),
(7602, 'system.permission-group.store', 14, NULL, NULL, NULL),
(7603, 'system.permission-group.edit', 14, NULL, NULL, NULL),
(7604, 'system.permission-group.update', 14, NULL, NULL, NULL),
(7605, 'system.setting.index', 14, NULL, NULL, NULL),
(7606, 'system.setting.update', 14, NULL, NULL, NULL),
(7607, 'system.activity-log.index', 14, NULL, NULL, NULL),
(7608, 'system.activity-log.show', 14, NULL, NULL, NULL),
(7609, 'system.staff.auth-sessions', 14, NULL, NULL, NULL),
(7610, 'system.staff.delete-auth-sessions', 14, NULL, NULL, NULL),
(7641, 'system.payment-methods.index', 1, NULL, NULL, NULL),
(7642, 'system.payment-methods.show', 1, NULL, NULL, NULL),
(7643, 'system.payment-methods.destroy', 1, NULL, NULL, NULL),
(7644, 'system.payment-methods.show', 1, NULL, NULL, NULL),
(7645, 'system.payment-methods.create', 1, NULL, NULL, NULL),
(7646, 'system.payment-methods.store', 1, NULL, NULL, NULL),
(7647, 'system.payment-methods.edit', 1, NULL, NULL, NULL),
(7648, 'system.payment-methods.update', 1, NULL, NULL, NULL),
(7649, 'system.staff.index', 1, NULL, NULL, NULL),
(7650, 'system.staff.show', 1, NULL, NULL, NULL),
(7651, 'system.staff.create', 1, NULL, NULL, NULL),
(7652, 'system.staff.store', 1, NULL, NULL, NULL),
(7653, 'system.staff.edit', 1, NULL, NULL, NULL),
(7654, 'system.staff.update', 1, NULL, NULL, NULL),
(7655, 'system.permission-group.index', 1, NULL, NULL, NULL),
(7656, 'system.permission-group.show', 1, NULL, NULL, NULL),
(7657, 'system.permission-group.create', 1, NULL, NULL, NULL),
(7658, 'system.permission-group.store', 1, NULL, NULL, NULL),
(7659, 'system.permission-group.edit', 1, NULL, NULL, NULL),
(7660, 'system.permission-group.update', 1, NULL, NULL, NULL),
(7661, 'system.setting.index', 1, NULL, NULL, NULL),
(7662, 'system.setting.update', 1, NULL, NULL, NULL),
(7663, 'system.activity-log.index', 1, NULL, NULL, NULL),
(7664, 'system.activity-log.show', 1, NULL, NULL, NULL),
(7665, 'system.staff.auth-sessions', 1, NULL, NULL, NULL),
(7666, 'system.staff.delete-auth-sessions', 1, NULL, NULL, NULL),
(7667, 'system.orders.index', 1, NULL, NULL, NULL),
(7668, 'system.orders.show', 1, NULL, NULL, NULL),
(7669, 'system.orders.edit', 1, NULL, NULL, NULL),
(7670, 'system.orders.update', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
CREATE TABLE `permission_groups` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_supervisor` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `whitelist_ip` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_groups`
--

INSERT INTO `permission_groups` (`id`, `name`, `is_supervisor`, `whitelist_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'yes', NULL, '2017-09-20 12:11:59', '2018-02-03 18:57:38', NULL),
(2, 'wewewe', 'no', 'wewew', '2014-08-15 22:00:00', '2014-08-15 22:00:00', NULL),
(13, 'admin', 'yes', NULL, '2020-08-17 23:52:12', '2020-08-17 23:52:12', NULL),
(14, 'aaaaaaa', 'yes', NULL, '2020-10-31 19:42:29', '2020-10-31 19:42:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `shown_name_ar` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shown_name_en` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_list` text COLLATE utf8mb4_unicode_ci,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `is_visible` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`name`, `value`, `shown_name_ar`, `shown_name_en`, `input_type`, `option_list`, `group_name`, `sort`, `is_visible`, `created_at`, `updated_at`) VALUES
('company_name', 'Delivery Runner', 'اسم الموقع', 'company name', 'text', NULL, 'system', 100, 'yes', '2017-12-13 22:00:00', '2020-11-01 06:21:11'),
('email', 'info@deliveryrunner.com', 'البريد الالكتروني', 'E-mail', 'text', NULL, 'contact', 12, 'yes', '2017-12-13 22:00:00', '2020-11-01 06:21:11'),
('facebook', 'https://facebook.com', 'صفحة الفيسبوك', 'facebook page', 'text', NULL, 'contact', 101, 'yes', '2020-08-14 22:00:00', '2020-11-01 06:21:11'),
('font_size', '14', 'حجم خط الجداول', 'font size', 'text', NULL, 'style', 1, 'yes', '2017-12-13 22:00:00', '2020-11-01 06:21:11'),
('font_weight', 'normal', 'سمك خط الجداول', 'table font weight', 'select', 'a:2:{s:6:\"normal\";s:6:\"normal\";s:4:\"bold\";s:4:\"bold\";}', 'style', 0, 'yes', NULL, '2020-11-01 06:21:11'),
('instagram', 'https://www.instagram.com/', 'انستجرام', 'Instagram', 'text', NULL, 'contact', 101, 'yes', '2020-08-14 22:00:00', '2020-11-01 06:21:11'),
('linkedin', 'https://www.linkedin.com/feed/', 'لينكد ان', 'Linkedin', 'text', NULL, 'contact', 101, 'yes', '2020-08-14 22:00:00', '2020-11-01 06:21:11'),
('menu_font_size', '14', 'حجم الخط القائمه', 'Menu font size', 'text', NULL, 'style', 2, 'yes', NULL, '2020-11-01 06:21:11'),
('menu_font_weight', 'normal', 'سمك خط القائمه', 'Menu font weight', 'select', 'a:2:{s:6:\"normal\";s:6:\"normal\";s:4:\"bold\";s:4:\"bold\";}', 'style', 0, 'yes', NULL, '2020-11-01 06:21:11'),
('mobile', '0100000000', 'الهاتف الارضي', 'Phone', 'text', NULL, 'contact', 3, 'yes', '2017-12-13 22:00:00', '2020-11-01 06:21:11'),
('point_value', '2', 'Point Value', 'Point Value', 'text', NULL, 'system', 100, 'yes', '2017-12-13 22:00:00', '2020-11-01 06:21:11'),
('twitter', 'https://twitter.com/home?lang=ar', 'تويتر', 'Twitter', 'text', NULL, 'contact', 101, 'yes', '2020-08-14 22:00:00', '2020-11-01 06:21:11'),
('youtube', 'https://youtube.com', 'قناة اليوتيوب', 'youtube channel', 'text', NULL, 'contact', 101, 'yes', '2020-08-14 22:00:00', '2020-11-01 06:21:11');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','sent','error') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `response` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `national_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','in-active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `language` enum('ar','en') COLLATE utf8mb4_unicode_ci DEFAULT 'en',
  `permission_group_id` int(10) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `firstname`, `lastname`, `email`, `mobile`, `avatar`, `gender`, `birthdate`, `national_id`, `address`, `password`, `description`, `job_title`, `status`, `language`, `permission_group_id`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Amr', 'Bdreldin', 'amr.bdreldin@osouly.com', '01014778866', NULL, 'male', '2019-05-01', '65432123456789', 'Haram', '$2y$10$ZuHTl6T15lxcHtwe09XLcOyp9Vb.vP6UIvnVipBLglyGHuoHhVaGy', 'aaa', 'Sales', 'active', 'en', 1, NULL, '2019-05-10 10:59:04', '2020-11-04 12:36:47', NULL),
(2, 'Mohmed', 'Nukeesh', 'mohamed@osouly.com', '11111111111', NULL, 'male', '2020-07-30', '11111111111111', 'Yted', '$2y$10$h2keYyGimMolzt74LpOXseqDg50OE7YI23k/Lw5Kcvm/RR0neke5a', '', 'Manager', 'active', 'ar', 13, NULL, '2020-08-21 17:21:25', '2020-11-01 06:12:56', NULL),
(3, 'Abdelrahman', 'Saeed', 'abdelrahmansaeed3@gmail.com', '01117414966', NULL, 'male', '1993-04-12', '', NULL, '$2y$10$ZuHTl6T15lxcHtwe09XLcOyp9Vb.vP6UIvnVipBLglyGHuoHhVaGy', NULL, 'Developer', 'active', 'ar', 1, NULL, '2020-09-10 22:32:23', '2020-10-11 21:37:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `type` enum('person','company') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgot_password_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_copy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` double NOT NULL DEFAULT '0',
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` int(11) DEFAULT NULL,
  `address_notes` int(11) DEFAULT NULL,
  `contract_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive','new') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `verified_at` datetime DEFAULT NULL,
  `firebase_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type`, `parent_id`, `name`, `email`, `mobile`, `username`, `password`, `activation_code`, `forgot_password_code`, `rc_copy`, `credit`, `lat`, `lng`, `address`, `address_notes`, `contract_image`, `bank_account`, `contact_name`, `plan_id`, `status`, `verified_at`, `firebase_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'company', 0, 'mac', 'amr@sds.sds', '01014778866', '', '$2y$10$nGXMtKCdMlccDNQDWGASFuwRuaVlWWxz4ewi6.szenugXwOlOcaD6', '6020', '', '2020/10/19/UEHWhFKIsxnVOtj4vsfS1ZPTfkRidsa11ZcP5IXi.png', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'active', NULL, NULL, '2020-10-18 22:27:27', '2020-10-19 00:27:27', NULL),
(2, 'person', 0, 'mac', 'amr@sds.sd', '01014778861', '', '$2y$10$W7p7Tqz4h6AAJCeJbv6ydewAYKAHrLwv8bT1yz4kW950fBJHdUQbS', '2601', NULL, '2020/10/19/foNUU33KL01bbSmj1XW5R0EMlqBOnnQn1eQgLO1q.png', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'active', NULL, NULL, '2020-10-31 22:08:40', '2020-10-19 00:08:52', NULL),
(3, 'company', 4, 'amr', 'amr@sds.sr', '01014778862', '', '$2y$10$D2K1nAGy9SWvjDeScbTRseEZ7yaTDy.ePUuxDbhtp7a1DFKjyOhi2', '6785', NULL, '2020/10/19/NLc2hDC5tGFgOWnYk2tOS7R14I76dxTlBS9nniUH.png', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'inactive', NULL, NULL, '2020-10-21 14:26:43', '2020-10-19 00:12:37', NULL),
(4, 'person', 0, 'amr2', 'amr@sds.sc', '01014778865', '01014778865', '$2y$10$oj/zXLuBl47PLVy3/gxe/u7JRnqbUHv6cM7eEeHmTWrbDpblti7Xq', '1577', NULL, NULL, 0, '30.162169', '31.327798', NULL, NULL, NULL, NULL, NULL, 1, 'active', '2020-10-19 00:17:11', NULL, '2020-10-31 22:08:36', '2020-10-19 00:17:11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `log_name` (`log_name`),
  ADD KEY `subject_type` (`subject_type`(191)),
  ADD KEY `causer_id` (`causer_id`),
  ADD KEY `causer_type` (`causer_type`(191)),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `auth_api`
--
ALTER TABLE `auth_api`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token_2` (`access_token`),
  ADD KEY `guard_name` (`guard_name`),
  ADD KEY `access_token` (`access_token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_session`
--
ALTER TABLE `auth_session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token_2` (`access_token`),
  ADD KEY `guard_name` (`guard_name`),
  ADD KEY `access_token` (`access_token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `auth_api`
--
ALTER TABLE `auth_api`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `auth_session`
--
ALTER TABLE `auth_session`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7671;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
