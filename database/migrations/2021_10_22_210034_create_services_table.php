<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string("image");
            $table->string('title_ar',255);
            $table->string('title_en',255);
            $table->string('title_ur',255);
            $table->string('title_bn',255)->nullable();
            $table->string('descreption_ar',255);
            $table->string('descreption_en',255);
            $table->string('descreption_ur',255);
            $table->string('descreption_bn',255)->nullable();
            $table->string('slug',255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
