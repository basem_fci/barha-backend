<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("order_status_id");
            $table->unsignedInteger("driver_id")->nullable();
            $table->unsignedInteger("city_id_from");
            $table->unsignedInteger("city_id_to");
             $table->unsignedInteger("district_id_from");
            $table->unsignedInteger("district_id_to");
            $table->unsignedInteger("user_id");
            $table->string('cargo_weight');
            $table->string('payload_type');
            $table->string('price');
            $table->string('total_price');
            $table->string('tracking_number')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('order_status_id')->references('id')->on('order_statuses');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('driver_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
