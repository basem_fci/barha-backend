<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestomnialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testomnials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image',255);
            $table->string('name',255);
            $table->text('descreption_ar',255);
            $table->text('descreption_en',255);
            $table->text('descreption_ur',255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testomnials');
    }
}
