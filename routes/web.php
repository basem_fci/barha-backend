<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index')->name('web.index');
Route::get('/login', 'ProfileController@Login')->name('web.login');

Route::get('forget-password','ProfileController@showForgetPasswordForm')->name('forget.password.get');
Route::post('forget-password', 'ProfileController@submitForgetPasswordForm')->name('forget.password.post');
Route::get('reset-password/{token}','ProfileController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'ProfileController@submitResetPasswordForm')->name('reset.password.post');

Route::get('/register', 'ProfileController@Register')->name('web.register');
Route::post('/register-driver', 'ProfileController@RegisterDriverPost')->name('web.register-driver-post');
Route::post('/register-company', 'ProfileController@RegisterCompanyPost')->name('web.register-company-post');
Route::post('/login', 'ProfileController@loginPOst')->name('web.login-post');
Route::get('/edit-profile', 'ProfileController@EditProfile')->name('web.edit-profile')->middleware('auth');
Route::PATCH('/update-profile/{id}', 'ProfileController@UpdateProfile')->name('web.update-profile')->middleware('auth');
Route::PATCH('/change-password', 'ProfileController@ChangePassword')->name('web.change-password')->middleware('auth');
Route::get('/logout', 'ProfileController@logout')->name('logout'); //


Route::get('/about-us', 'PagesController@About')->name('web.about-us');
Route::get('/services', 'PagesController@services')->name('web.services');
Route::get('/service-details/{slug}', 'PagesController@serviceDetails')->name('web.service-details');
Route::get('/contact-us', 'PagesController@ContactUs')->name('web.contact-us');
Route::post('/contact-us', 'PagesController@ContactUsPOst')->name('web.contact-us-post');
Route::get('/privacy-policy', 'PagesController@privacyPolicy')->name('web.privacy-policy');
Route::get('/terms-conditions', 'PagesController@TermsConditions')->name('web.terms-conditions');
Route::get('orders', 'OrderController@orders')->name('web.orders');
Route::post('orders', 'OrderController@storeOrders')->name('web.orders.store');
Route::PATCH('orders/update/{id}', 'OrderController@updateOrders')->name('web.orders.update');
Route::get('order-details/{tracking_number}', 'OrderController@orderDetails')->name('web.order-details');
Route::Post('add-offer', 'OrderController@addOffer')->name('web.add-offer');
Route::get('tracking/{tracking_number}', 'OrderController@tracking')->name('web.tracking');
Route::get('confirm-order/{tracking_number}', 'OrderController@confirmOrder')->name('web.confirm-order');
Route::get('delivered-order/{tracking_number}', 'OrderController@deliveredOrder')->name('web.delivered-order');
Route::get('confirm-delivered-order/{tracking_number}', 'OrderController@confirmDeliveredOrder')->name('web.confirm-delivered-order');
Route::get('return-order/{tracking_number}', 'OrderController@returnOrder')->name('web.return-order');
Route::get('reship-order/{tracking_number}', 'OrderController@reshipOrder')->name('web.reship-order');
Route::get('accept-offer/{tracking_number}', 'OrderController@acceptOffer')->name('web.accept-offer');
Route::get('cancel-offer/{id}', 'OrderController@cancelOffer')->name('web.cancel-offer');
Route::get('/ajax','OrderController@index')->name('web.misc.ajax'); //
Route::get('/language','OrderController@language')->name('web.language.change'); //
Route::get('/update-location','OrderController@updateLocation')->name('web.update-location'); //

Route::get('/dashboard', 'UserController@Dashboard')->name('web.dashboard');
Route::get('/edit-profile', 'ProfileController@EditProfile')->name('web.edit-profile')->middleware('auth');
Route::PATCH('/change-password', 'ProfileController@ChangePassword')->name('web.change-password')->middleware('auth');

Route::get('/district/{id}','OrderController@district')->name('web.districtle');
Route::get('/insert-cities','WebController@InsertCity')->name('web.insert.cities');
